import React from 'react';
import ReactDOM from 'react-dom';
import thunkMiddleware from 'redux-thunk'
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

// import postListReducer from './user_view/post_list/postListReducer';
import UserReducer from './user_view/rootReducer'

import PostList from './user_view/post_list/PostList';
import About from './user_view/about/About';
import Post from './user_view/post/Post';
import Mypage from './user_view/mypage/Mypage';
import App from './user_view/App';
import WriteComment from './user_view/write_comment/WriteComment'

import OwnerApp from './owner_view/new/main/App';
import OwnerOrderPage from './owner_view/new/order/OrderPage';
import OwnerCouponPage from './owner_view/new/couponList/CouponPage';
import OwnerStatisticPage from './owner_view/new/statistic/StatisticPage';
import OwnerCouponGeneratePage from './owner_view/new/couponGenerate/CouponGeneratePage'
import OwnerStorePage from './owner_view/new/store/StorePage';


import OwnerHome from './owner_view/page/Home';
import OwnerHeader from './owner_view/page/Header';
import firebase from 'firebase';
import Create_coupon from './owner_view/page/Create_coupon';
import User_list from './owner_view/page/User_list';
import Coupon_list from './owner_view/page/Coupon_list';
import Contact from './owner_view/page/Contact';

import AdminHome from './admin_view/page/Home';
import AdminHeader from './admin_view/page/Header';
import PostUpload from './admin_view/page/PostUpload';
// import Create_coupon from './owner_view/page/Create_coupon';
// import User_list from './owner_view/page/User_list';
// import Coupon_list from './owner_view/page/Coupon_list';
// import Contact from './owner_view/page/Contact';

import {Router, Route, Link, browserHistory, IndexRoute} from 'react-router'

let rootElement = document.getElementById('user_root');
let ownerElement = document.getElementById('owner_root');
let adminElement = document.getElementById('admin_root');

const userStore = createStore(UserReducer, composeWithDevTools(
    applyMiddleware(thunkMiddleware),
    // other store enhancers if any
));
// const createStoreWithMiddleware = applyMiddleware(
//     thunkMiddleware
// )(createStore);
// const userStore = (window.devToolsExtension ? window.devToolsExtension()(createStoreWithMiddleware) : createStoreWithMiddleware)(UserReducer);
userStore.subscribe(function(){
    console.log(userStore.getState());
})
if (rootElement != null) {
    ReactDOM.render(
        <Provider store={userStore}>
            <Router history={browserHistory}>
                <Route path="/" component={App}>
                    <IndexRoute component={PostList}/>
                    <Route path="post" component={PostList}/>
                    <Route path="post/:id" component={Post}/>
                    <Route path="about" component={About}/>
                    <Route path="mypage" component={Mypage}/>
                    <Route path="comment" component={WriteComment}/>
                    {/*<Route path="404" component={UnknownPage}/>*/}
                </Route>
            </Router>
        </Provider>, rootElement);
} else if (ownerElement != null) {
    // var config = {
    //     apiKey: "AIzaSyCyBiYDnrSfuMvgdzxrv1TN50TDQrMpUs0",
    //     authDomain: "seereal-2016.firebaseapp.com",
    //     databaseURL: "https://seereal-2016.firebaseio.com",
    //     storageBucket: "seereal-2016.appspot.com",
    // };
    // firebase.initializeApp(config);
    ReactDOM.render(
        <Router history={browserHistory}>
            {/*<OwnerHeader />*/}
            <Route path="/owner" component={OwnerApp}>
                <IndexRoute component={OwnerOrderPage}/>
                <Route path="order" component={OwnerOrderPage}/>
                <Route path="make" component={OwnerCouponGeneratePage}/>
                <Route path="store" component={OwnerStorePage}/>
                <Route path="coupon" component={OwnerCouponPage}/>
                <Route path="statistic" component={OwnerStatisticPage}/>
            </Route>

        </Router>, ownerElement);
} else {
    ReactDOM.render(
        <Router history={browserHistory}>
            <OwnerHeader/>
            <Route path="/admin" component={AdminHeader}>
                <IndexRoute component={AdminHome}/>
                <Route path="home" component={AdminHome}/>
                <Route path="post_upload" component={PostUpload}/>
                {/*<Route path="coupon_create" component={Create_coupon}/>*/}
                {/*<Route path="users" component={User_list}/>*/}
                {/*<Route path="coupon" component={Coupon_list}/>*/}
                {/*<Route path="contact" component={Contact}/>*/}
            </Route>
        </Router>, adminElement);
}
