import React from 'react';
import StatusBox from './StatusBox';
import CSSModules from 'react-css-modules';
import styles from './StatisticsPage.css';

class StatisticPage extends React.Component{
    render(){
        return (
            <div styleName="wrapper">
                <StatusBox/>
            </div>
        );
    }
}

export default CSSModules(StatisticPage,styles);