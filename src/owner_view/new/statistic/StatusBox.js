import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './StatusBox.css';

class RealtimeStatusBox extends React.Component {
    render() {
        return (
            <div styleName="wrapper">
                <div styleName="left-area">
                    <div styleName="menu-title">바나바나 메뉴</div>
                    <div styleName="statistic">
                        오늘 총 <span styleName="count">24개</span> 판매되었습니다.
                    </div>
                    <div styleName="statistic">
                        이번달 총 <span styleName="count">194개</span> 판매되었습니다.
                    </div>
                </div>
                <div styleName="right-area">
                    이번 달 총 매출 <span styleName="price">W 25,730,100</span>
                </div>
            </div>
        );
    }
}

export default CSSModules(RealtimeStatusBox, styles);