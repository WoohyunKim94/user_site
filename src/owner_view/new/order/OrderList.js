import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './OrderList.css';
import OrderItem from './OrderItem';

class OrderList extends React.Component{
    render(){
        return (
          <div styleName="scroll-area">
              {this.props.items.map((item, index)=>{
                  return (
                      <OrderItem key={index}/>
                  );
              })}
          </div>
        );
    }
}
export default CSSModules(OrderList,styles);

