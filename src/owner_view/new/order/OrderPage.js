import React from 'react';
import OrderList from './OrderList';

export default class OrderPage extends React.Component{
    render(){
        return (
          <div >
            <OrderList items={[1,2,3,4,5,6,7,8,9]}/>
          </div>
        );
    }
}