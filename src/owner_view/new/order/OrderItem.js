import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './OrderItem.css';

class OrderItem extends React.Component{
    render(){
        return (
          <div styleName="item-wrapper">
              <div styleName="left-area">
                  <div styleName="time">
                      13시 45분
                  </div>
              </div>
              <div styleName="middle-area">
                  <span styleName="food-name">짬뽕</span><span styleName="food-count">x 1</span>
              </div>
              <div styleName="right-area">
                  <div styleName="button">
                      <div styleName="upper-info">
                          <span>주문 수락</span>
                      </div>
                      <div styleName="bottom-info">
                          주문 수락을 하시면 취소할 수 없습니다
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}
export default CSSModules(OrderItem,styles);

