import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './CouponPage.css'
import {Link} from 'react-router';

class CouponPage extends React.Component {
    render() {
        return (
            <div styleName="wrapper">
                <div styleName="menu-list">
                    <div styleName="title">
                        히든 메뉴
                    </div>
                    <div styleName="menu-list-items">
                        {/*<div styleName="menu-item" style={{borderRight:'5px solid  #40c2d3'}}>바라바라 핫</div>*/}
                        {/*<div styleName="menu-item">불타는 떡볶이</div>*/}
                        {/*<div styleName="menu-item">굳데이 할로윈 빙수</div>*/}
                        {/*<div styleName="menu-item">와와우와</div>*/}
                        <div styleName="empty">
                            <div styleName="empty-content">
                                <Link to="/owner/make" >버튼</Link>
                                <p>히든메뉴가 없습니다</p>
                                <p>히든 메뉴를 만들어주세요</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div styleName="menu-preview">
                    <div styleName="overlay"></div>
                    <div styleName="title">
                        미리보기
                    </div>
                    <div styleName="preview">
                        미리보기
                    </div>
                    <div styleName="buttons">
                        <button styleName="delete">삭제하기</button>
                        <button styleName="edit">수정하기</button>
                    </div>
                </div>
            </div>
        );
    }
}
export default CSSModules(CouponPage,styles);