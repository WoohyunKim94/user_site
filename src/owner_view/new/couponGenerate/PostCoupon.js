import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './PostCoupon.css';

class PostCoupon extends React.Component {
    constructor() {
        super();
        this.state = {
            count: 1,
            phoneCheck: false
        };
        this.countPlus = this.countPlus.bind(this);
        this.countMinus = this.countMinus.bind(this);
        this.setPhone = this.setPhone.bind(this);
    }

    countPlus() {
        if( this.state.count + 1>this.props.people){
            alert('최대인원을 초과했습니다');
            return;
        }
        this.setState({
            count: this.state.count + 1
        });
    }

    countMinus() {
        if (this.state.count != 1) {
            this.setState({
                count: this.state.count - 1
            });
        }
    }

    setPhone(event) {
        if (event.target.value.length == 11) {
            this.setState({
                phoneCheck: true
            });
            this.coupon.setPhone(event.target.value);
        }else{
            this.setState({
                phoneCheck: false
            });
        }
    }

    reserve() {

    }

    render() {
        return (
                <div styleName="wrapper">
                    <div styleName="imageArea" onClick={this.props.onClick}>
                        <img src={this.props.image}/>
                        <div styleName="textArea">
                            <div styleName="inlineAreaLeft">
                                <div styleName="bigText">
                                    <span styleName="fontLight">{this.props.title}</span>
                                </div>
                                <div styleName="smallText">
                                    <span styleName="fontLight">{this.props.tags}</span>
                                </div>
                            </div>
                            <div styleName="inlineAreaRight">
                                <div styleName="smallText">
                                </div>
                                <div styleName="bigText">
                                    <span styleName="fontBold">{this.props.price}원</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        styleName="open-area formArea">
                        <div styleName="description" dangerouslySetInnerHTML={{__html: this.props.description}}></div>
                        <div styleName="datetime">
                            <div styleName="minus" onClick={this.countMinus}></div>
                            <div styleName="number">{this.state.count}명</div>
                            <div styleName="plus" onClick={this.countPlus}></div>
                        </div>
                        <div styleName="phone">
                            <input type="text" onChange={this.setPhone} maxLength="11" minLength="11"
                                   placeholder="전화번호를 입력해 주세요 (- 제외)"/>
                            <div styleName={this.state.phoneCheck?"phone-check":''}></div>
                        </div>
                        <div styleName="datetime submit-btn">
                            <button onClick={this.reserve}>예약하기</button>
                        </div>
                    </div>
                </div>
        );
    }
}

module.exports = CSSModules(PostCoupon, styles, {allowMultiple: true});