import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './TextInput.css';

class TextInput extends React.Component {
    render() {
        return (
            <div class={this.props.className} styleName="wrapper">
                <input id={this.props.id} onChange={this.props.onChange} step={this.props.step} min="0" type={this.props.type} placeholder={this.props.placeholder}/>
            </div>
        );
    }
}

export default CSSModules(TextInput,styles);