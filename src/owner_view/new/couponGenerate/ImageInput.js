import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './ImageInput.css';

class ImageInput extends React.Component {
    constructor(){
        super();
        this.onFileChange=this.onFileChange.bind(this);
    }
    onFileChange(event){
        let output = document.getElementById('imageUploadForm');
        this.props.onImageSelect(URL.createObjectURL(event.target.files[0]));
    }
    render() {
        return (
            <div class={this.props.className} styleName="wrapper">
                <label for="imageUploadForm">이미지 svg</label>
                <input id="imageUploadForm" type="file" onChange={this.onFileChange}/>
            </div>
        );
    }
}

export default CSSModules(ImageInput,styles);