import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './CouponGeneratePage.css';
import TextInput from './TextInput'
import ImageInput from './ImageInput'
import MenuTagInput from './MenuTagInput'
import PostCoupon from './PostCoupon';
import TextAreaInput from './TextAreaInput';
import NumberInput from './NumberInput';

class CouponGeneratePage extends React.Component {
    constructor() {
        super();
        this.state = {
            imageUrl: null,
            couponInfo: {}
        };
        this.onImageSelect = this.onImageSelect.bind(this);
        this.onTagSelect = this.onTagSelect.bind(this);
        this.onChange=this.onChange.bind(this);
    }

    onImageSelect(fileUrl) {
        this.onChange('image',fileUrl);
    }

    onTagSelect(idList, titleList) {
        this.onChange('tagIdList',idList,()=>{
            this.onChange('tagTitleList',titleList);
        });
    }
    onChange(type,value,cb){
        if(typeof(value)==='string'){
            value=value.trim();
        }
        if(type=='price'){
            value=new Intl.NumberFormat().format(value);
        }
        if(type=='description'){
            value=value.replace(/\r?\n/g, '<br />');
        }
        this.setState({
           couponInfo: Object.assign({},this.state.couponInfo,{
               [type]:value
           })
        },()=>{
            console.log(this.state.couponInfo);
            if(cb)cb();
        });
    }
    render() {
        return (
            <div styleName="wrapper">
                <div styleName="menu-generate">
                    <div styleName="title">
                        히든 메뉴 만들기
                    </div>
                    <div styleName="input-group">
                        <TextInput styleName="text-input" type="text" id="name"
                                     placeholder="메뉴 이름을 적어주세요" onChange={v=>this.onChange('title',v.target.value)}/>
                        <ImageInput styleName="text-input" onImageSelect={this.onImageSelect} />
                        <TextAreaInput styleName="text-input" placeholder="메뉴 설명을 입력하세요" id="description" onChange={v=>this.onChange('description',v.target.value)}/>
                        <TextInput styleName="text-input" type="number" id="price"  step="1000"
                           placeholder="메뉴 가격을 입력하세요" onChange={v=>this.onChange('price',v.target.value)}/>
                        <NumberInput styleName="number-input" placeholder="최대 이용가능 인원을 입력하세요" onChange={v=>this.onChange('number',v)}/>
                        <MenuTagInput styleName="tag-input" onTagSelect={this.onTagSelect} tags={[{id:1,title:"맛있는"},{id:2,title:"더맛있는"},{id:3,title:"별로인"}]}/>
                        <button>쿠폰 만들기</button>
                    </div>
                </div>
                <div styleName="menu-preview">
                    <div styleName="title">
                        미리보기
                    </div>
                    <div styleName="preview">
                        <PostCoupon {...this.state.couponInfo}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default CSSModules(CouponGeneratePage, styles);