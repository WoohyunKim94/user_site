import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './MenuTagInput.css';

class MenuTagInput extends React.Component {
    constructor() {
        super();
        this.onTagSelect = this.onTagSelect.bind(this);
        this.state = {
            tagSelectIdList: [],
            tagSelectTitleList: [],
            currentCount:0
        };
        this.bgStyle = {
            background: '#40c2d3',
            color: 'white'
        }
    }

    onTagSelect(tagId, tagTitle) {
        let index = this.state.tagSelectIdList.indexOf(tagId);
        if (index != -1) {
            this.setState({
                tagSelectIdList: [...this.state.tagSelectIdList.slice(0,index),...this.state.tagSelectIdList.slice(index+1)],
                tagSelectTitleList: [...this.state.tagSelectTitleList.slice(0,index),...this.state.tagSelectTitleList.slice(index+1)]
            },()=>{
                this.setState({
                    currentCount: --this.state.currentCount
                },()=>{
                    this.props.onTagSelect(this.state.tagSelectIdList,this.state.tagSelectTitleList);
                })
            });

        }else{
            if(this.state.tagSelectIdList.length==3){
                alert('태그는 3개까지만 선택할 수 있습니다');
                return;
            }
            this.setState({
                tagSelectIdList: [...this.state.tagSelectIdList,tagId],
                tagSelectTitleList: [...this.state.tagSelectTitleList,tagTitle]
            },()=>{
                this.setState({
                    currentCount: ++this.state.currentCount
                },()=>{
                    this.props.onTagSelect(this.state.tagSelectIdList,this.state.tagSelectTitleList);
                });
            });
        }

    }

    render() {
        return (
            <div class={this.props.className} styleName="wrapper">
                <div styleName="placeholder">
                    <span styleName="title">메뉴를 설명할 태그를 최대 3개까지 선택해주세요</span>
                    <span styleName="count-status">{this.state.currentCount}/3</span>
                </div>
                <div styleName="tag-list">
                    {
                        this.props.tags.map((tag, index)=> {
                            return (
                                <div styleName="tag-item"
                                     style={(this.state.tagSelectIdList.indexOf(tag.id)!=-1 ? this.bgStyle : null)}
                                     onClick={this.onTagSelect.bind(this, tag.id, tag.title)} key={index}>{tag.title}</div>
                            );
                        }, this)
                    }
                </div>
            </div>
        );
    }
}

export default CSSModules(MenuTagInput, styles);