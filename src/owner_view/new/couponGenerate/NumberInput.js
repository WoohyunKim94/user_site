import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './NumberInput.css';

class NumberInput extends React.Component {
    constructor(){
        super();
        this.state={
            number:1
        }
        this.numberChange=this.numberChange.bind(this);
    }
    numberChange(type){
        switch (type){
            case "PLUS":
                this.setState({
                    number: ++this.state.number
                },()=>{
                    this.props.onChange(this.state.number.toString())
                })
                break;
            case "MINUS":
                this.setState({
                    number: this.state.number>1?--this.state.number:this.state.number
                },()=>{
                    this.props.onChange(this.state.number.toString())
                })
                break;
            default:
                break;
        }
    }
    render() {
        return (
            <div class={this.props.className} styleName="wrapper">
                <div styleName="minus-btn" onClick={this.numberChange.bind(this,"MINUS")}></div>
                <div styleName="number-display">
                    <div styleName="number">{this.state.number}명</div>
                    <div styleName="placeholder">{this.props.placeholder}</div>
                </div>
                <div styleName="plus-btn" onClick={this.numberChange.bind(this,"PLUS")}></div>
                {/*<input id={this.props.id} onChange={this.props.onChange} step={this.props.step} min="0" type={this.props.type} placeholder={this.props.placeholder}/>*/}
            </div>
        );
    }
}

export default CSSModules(NumberInput,styles);