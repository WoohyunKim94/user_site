import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './TextAreaInput.css';

class TextAreaInput extends React.Component {
    render() {
        return (
            <div class={this.props.className} styleName="wrapper">
                <textarea id={this.props.id} onChange={this.props.onChange} placeholder={this.props.placeholder}/>
            </div>
        );
    }
}

export default CSSModules(TextAreaInput,styles);