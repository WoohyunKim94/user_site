import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './StoreItem.css';

class StoreItem extends React.Component {
    constructor() {
        super();

    }
    render() {
        return (
            <div styleName="wrapper" class={this.props.className}>
                <div styleName="content-wrapper">
                    <div styleName="title">
                        회원관리 모듈
                    </div>
                    <div styleName="icon">

                    </div>
                    <div styleName="price">
                        W 10,000원 / 월
                    </div>
                    <div styleName="description">
                        회원관리 모듈 이리저리 헬로우 합니다
                    </div>
                </div>
                <div styleName="buy-btn" onClick={this.props.onClick}>
                    구매하기
                </div>
            </div>
        );
    }
}
export default CSSModules(StoreItem, styles);