import React from 'react';
import Modal from 'react-responsive-modal'
import styles from './PaymentConfirmModal.css'
import CSSModules from 'react-css-modules';

class PaymentConfirmModal extends React.Component {
    constructor() {
        super();
        this.onConfirm = this.onConfirm.bind(this);
        this.onDeny = this.onDeny.bind(this);
    }

    onConfirm() {
        this.props.paymentModal(true);
    }

    onDeny() {
        this.props.paymentModal(false);
    }

    render() {
        return (
            <Modal open={this.props.openPaymentConfirmModal} onClose={this.onDeny} showCloseIcon={false}
                   modalStyle={{width: '50vw', padding: 0}}
                   little>
                <div styleName="wrapper">
                    <div styleName="upper-area">
                        <div styleName="item-brief">
                            <div styleName="title">회원관리 모듈</div>
                            <div styleName="icon">아이콘</div>
                            <div styleName="price">10,000원 / 년</div>
                        </div>
                        <div styleName="item-specific">
                            <div styleName="duration">
                                아이템 적용 기간은 <span styleName="date">10월 4일</span> 부터 <span styleName="date">11월 10일</span> 까지입니다.
                            </div>
                            <div styleName="description">
                                회원관리 모듈은 이리저리 헬로우 합니다 헬로우합니다 헬로우합니다 은 이리저리 헬로우합니다 헬로우합니다 헬로우합니다
                            </div>
                        </div>
                    </div>
                    <div styleName="bottom-area">
                        <button onClick={this.onDeny} style={{background: 'rgba(0,0,0,.4)'}}>취소하기</button>
                        <button onClick={this.onConfirm} style={{background: '#40c2d3'}}>구입하기</button>
                    </div>
                </div>
            </Modal>
        )
    }
}
export default CSSModules(PaymentConfirmModal, styles);


