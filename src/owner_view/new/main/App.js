import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './App.css';
import {Link} from 'react-router'
import LoginPage from '../login/LoginPage';
import RegisterPage from '../register/RegisterPage'
require('disable-react-devtools');

class App extends React.Component {
    constructor() {
        super();

        this.state = {
            logined: true,
            currentPage: 'login',
            currentMenu: (location.pathname.split('/')[2] ? location.pathname.split('/')[2] : 'order')
        };
        this.clickMenu = this.clickMenu.bind(this);
        this.goTo = this.goTo.bind(this);
    }

    clickMenu(menuName) {
        this.setState({
            currentMenu: menuName
        });
    }

    goTo(location) {
        this.setState({
            currentPage: location
        })
    }

    render() {
        if (!this.state.logined) {
            let currentPage;
            switch (this.state.currentPage) {
                case 'login':
                    currentPage = <LoginPage/>;
                    break;
                case 'register':
                    currentPage = <RegisterPage/>;
                    break;
            }
            return (
                <div styleName="unauthenticated-wrapper">
                    <div styleName="header">
                        <button styleName="register_button" onClick={this.goTo.bind(this,'register')}>회원가입</button>
                        <button styleName="login_button" onClick={this.goTo.bind(this,'login')}>로그인</button>
                    </div>
                    <div styleName="body">
                        {currentPage}
                    </div>
                </div>
            );
        }
        return (
            <div>
                <div styleName="top-banner">
                    <div styleName="top-menu logo">안녕하세요, 김진수 사장님!</div>
                    <div styleName="top-menu coupon-make-btn"><Link to="/owner/make" onClick={this.clickMenu.bind(this, 'coupon')}>메뉴
                        만들기</Link></div>
                </div>
                <div styleName="layout">
                    <div styleName="side-menu">
                        <ul>
                            <li>
                                <Link to="/owner/order" onClick={this.clickMenu.bind(this, 'order')}>
                                    <div styleName="menu"
                                         style={this.state.currentMenu == 'order' ? {background: '#40c2d3',color:'white'} : {}}>주문 현황
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/owner/coupon" onClick={this.clickMenu.bind(this, 'coupon')}>
                                    <div styleName="menu"
                                         style={this.state.currentMenu == 'coupon' ? {background: '#40c2d3',color:'white'} : {}}>히든 쿠폰
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/owner/statistic" onClick={this.clickMenu.bind(this, 'statistic')}>
                                    <div styleName="menu"
                                         style={this.state.currentMenu == 'statistic' ? {background: '#40c2d3',color:'white'} : {}}>주문
                                        통계
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/owner/store" onClick={this.clickMenu.bind(this, 'store')}>
                                    <div styleName="menu"
                                         style={this.state.currentMenu == 'store' ? {background: '#40c2d3',color:'white'} : {}}>스토어
                                    </div>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div styleName="main-area">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

export default CSSModules(App, styles,{allowMultiple: true});