import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './LoginPage.css';

class LoginPage extends React.Component {
    render() {
        return (
            <div styleName="wrapper">
                <div styleName="login-area">
                    <div styleName="title">로그인</div>
                    <div styleName="id">
                        <input type="text" placeholder="아이디"/>
                    </div>
                    <div styleName="pw">
                        <input type="password" placeholder="비밀번호"/>
                    </div>
                    <div styleName="find_pw">
                        비밀번호를 잃어버리셨나요? <a>비밀번호 찾기</a>
                    </div>
                    <div styleName="login-btn">
                        <button>로그인</button>
                    </div>
                </div>
            </div>
        );
    }
}
export default CSSModules(LoginPage, styles);