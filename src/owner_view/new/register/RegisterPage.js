import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './RegisterPage.css';
import Location from './Location'

class RegisterPage extends React.Component {
    render() {
        return (
            <div styleName="wrapper">
                <div styleName="login-area">
                    register
                    <Location/>
                </div>
            </div>
        );
    }
}
export default CSSModules(RegisterPage, styles);