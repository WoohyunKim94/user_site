import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './Location.css';
import fetch from 'isomorphic-fetch'
import ToggleDisplay from 'react-toggle-display';

class Location extends React.Component {
    constructor() {
        super();
        this.onAddressSelect = this.onAddressSelect.bind(this);
        this.onAddressClick = this.onAddressClick.bind(this);
        this.onGeneralLocationFinalize=this.onGeneralLocationFinalize.bind(this);
        this.onSpecificLocationFinalize=this.onSpecificLocationFinalize.bind(this);
        this.state = {
            locationList: [],
            openSearch:true,
            openLocationList: false,
            openMap: false,
            openSpecificLocation: false,
            finish:false,
            lat: '',
            lng: '',
            generalLocation:'',
            specificLocation:''
        }
    }

    onAddressSelect() {
        let locationQuery = document.getElementById("location").value;
        var option = {
            method: 'GET'
        };
        fetch('/api/owner/register/location?location=' + encodeURIComponent(locationQuery), option)
            .then(res=>res.json())
            .then(data=> {
                this.setState({
                    locationList: data.result.items,
                    openSearch: false,
                    openLocationList: true
                });
            });
    }

    onAddressClick(item) {
        this.setState({
            openLocationList: false,
            openMap: true,
            generalLocation: item.address,
            lat: item.point.y,
            lng: item.point.x
        });
        //해당 포인트를 지도에 표시해주고 이에대한 확인 받기
        var mapOptions = {
            center: new window.naver.maps.LatLng(item.point.y, item.point.x),
            zoom: 15
        };
        var map = new window.naver.maps.Map('map', mapOptions);
        var marker = new window.naver.maps.Marker({
            position: new window.naver.maps.LatLng(item.point.y, item.point.x),
            map: map
        });
        window.naver.maps.Event.addListener(map, 'click', function (e) {
            marker.setPosition(e.latlng);
            this.setState({
                lat: e.latlng._lat,
                lng: e.latlng._lng
            })
        }.bind(this));
    }
    onGeneralLocationFinalize(){
        this.setState({
            openMap: false,
            openSpecificLocation: true
        });
    }
    onSpecificLocationFinalize(){
        this.setState({
            openSpecificLocation: false,
            specificLocation: document.getElementById('specific_location').value,
            finish: true
        },()=>{
            console.group();
            console.log(this.state.generalLocation+" "+this.state.specificLocation);
            console.log("위도는 "+this.state.lat);
            console.log("경도는 "+this.state.lng);
            console.groupEnd();
        });

    }
    render() {
        return (
            <div styleName="wrapper">
                <ToggleDisplay show={this.state.openSearch}>
                    <label for="location">주소</label>
                    <input id="location" type="text"/>
                    <button onClick={this.onAddressSelect}>주소 확인</button>
                </ToggleDisplay>
                <ToggleDisplay show={this.state.openLocationList}>
                    <p>아래에서 맞는 주소를 클릭해주세요</p>
                    <ul>
                        {this.state.locationList.map((item, index)=> {
                            return (
                                <li key={index} onClick={this.onAddressClick.bind(this, item)}>{item.address}</li>
                            );
                        }, this)}
                    </ul>
                </ToggleDisplay>
                <ToggleDisplay show={this.state.openMap}>
                    <p>지도에 찍힌 점이 정확하지 않다면 지도를 클릭해서 지점을 바꿔주세요</p>
                    <div id="map" style={{width: '400px', height: '400px'}}></div>
                    <button onClick={this.onGeneralLocationFinalize}>정확합니다</button>
                </ToggleDisplay>
                <ToggleDisplay show={this.state.openSpecificLocation}>
                    "{this.state.generalLocation}" 뒤에 들어갈 세부 주소를 적어주세요
                    <input type="text" id="specific_location" placeholder="에) 몇층 몇호"/>
                    <button onClick={this.onSpecificLocationFinalize}>완료</button>
                </ToggleDisplay>
                <ToggleDisplay show={this.state.finish} >
                    <p>주소는 <b>{this.state.generalLocation+" "+this.state.specificLocation}</b></p>
                    <p>위도는 <b>{this.state.lat}</b></p>
                    <p>경도는 <b>{this.state.lng}</b></p>
                </ToggleDisplay>
            </div>

        );
    }
}
export default CSSModules(Location, styles);