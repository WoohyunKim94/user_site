import React from 'react';

class CouponInfo extends React.Component{
    render(){
        return (
            <div class="panel panel-blue">
                <div class="panel-heading dark-overlay">
                    <svg class="glyph stroked clipboard-with-paper">
                        <use xlinkHref="#stroked-clipboard-with-paper"></use>
                    </svg>
                    쿠폰 정보
                </div>
                <div class="panel-body">
                    <div class="well">
                        <div class="row">
                            {this.props.data.body}
                        </div>
                    </div>
                </div>
                <div class="panel-footer">

                    <button type="button" class="btn btn-primary btn-lg btn-block">결제완료</button>

                </div>
            </div>
        );
    }
}

module.exports=CouponInfo;