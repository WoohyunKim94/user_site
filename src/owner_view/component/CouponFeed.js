import React from 'react';

class CouponFeed extends React.Component {
    render() {
        return (
            <div class="panel panel-default chat">
                <div class="panel-heading" id="accordion">
                    <svg class="glyph stroked two-messages">
                        <use xlinkHref="#stroked-two-messages"></use>
                    </svg>
                    쿠폰 사용 현황
                </div>
                <div class="panel-body">
                    <ul>
                        {this.props.data.map(function (item, index) {
                            return (
                                <a key={index}>
                                    <li class="left clearfix">
                                        <span class="chat-img pull-left">
                                            <img src="http://placehold.it/80/30a5ff/fff" alt="User Avatar"
                                                 class="img-circle"/>
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">김우현</strong>
                                                <small class="text-muted">2016년 3월 4일 12시 50분</small>
                                            </div>
                                            <p>
                                                쿠폰: 삼겹살 2인분 20% 할인
                                            </p>
                                        </div>
                                    </li>
                                </a>
                            );
                        })}
                    </ul>
                </div>

                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-md"
                               placeholder="이름을 검색해주세요"/>
                        <span class="input-group-btn">
            <button class="btn btn-success btn-md" id="btn-chat">검색</button>
            </span>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = CouponFeed;