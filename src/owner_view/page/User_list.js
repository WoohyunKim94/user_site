import React from 'react';
import ListTable from '../component/ListTable';

export default class User_list extends React.Component {
    constructor() {
        super();
        this.state={
            userList :{
                header: [{key:'name',name: '유저 이름'}, {key:'coupon_body',name:'쿠폰 내용'}, {key:'time',name:'이용 시간'}],
                body: [{id: 10, name: '이름입니다' ,coupon_body:'쿠폰 내용입니다', time: "시간 입니다."}],
                config: {
                    search: true,
                    title: '유저',
                    sortBy: '이용시간',
                    sortOrder: 'desc',
                    search: true,
                    pagination: true,
                    toggle: true,
                    hasButton: [{name:'삭제',onclick: function(){
                        alert('dd');
                    }}]
                }
            }
        };
    }
    componentDidMount(){
        this.renderTable();
    }
    render() {
        return (
            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="#"><svg class="glyph stroked home"><use xlinkHref="#stroked-home"></use></svg></a></li>
                        <li class="active">Icons</li>
                    </ol>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">유저목록</h1>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <ListTable data={this.state.userList}/>
                    </div>
                </div>
            </div>
        );
    }
    renderTable(){
        $(function () {
            $('#hover, #striped, #condensed').click(function () {
                var classes = 'table';

                if ($('#hover').prop('checked')) {
                    classes += ' table-hover';
                }
                if ($('#condensed').prop('checked')) {
                    classes += ' table-condensed';
                }
                $('#table-style').bootstrapTable('destroy')
                    .bootstrapTable({
                        classes: classes,
                        striped: $('#striped').prop('checked')
                    });
            });
        });
        $('[data-toggle="table"]').bootstrapTable();
        function rowStyle(row, index) {
            var classes = ['active', 'success', 'info', 'warning', 'danger'];

            if (index % 2 === 0 && index / 2 < classes.length) {
                return {
                    classes: classes[index / 2]
                };
            }
            return {};
        }
    }
}