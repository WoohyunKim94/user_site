import React from 'react';
import {browserHistory} from 'react-router'
import CouponList from '../component/ListTable';

export default class Coupon_list extends React.Component {
    constructor() {
        super();
        this.state={
            couponList :{
                header: [{id:'id',name: '쿠폰 ID'}, {id:'name',name: '쿠폰 이름'}, {id:'body',name:'쿠폰 내용'}, {id:'count',name:'현재까지 이용 수'}],
                body: [{id:1,name:'쿠폰 이름',body: '질소과자 20% 할인', count: 3}],
                config: {
                    search: true,
                    title: '현재 쿠폰',
                    sortBy: 'name',
                    sortOrder: 'desc',
                    search: 'false',
                    pagination: 'false',
                    toggle: false
                }
            },
            deletedCouponList :{
                header: [{id:'id',name: '쿠폰 ID'}, {id:'name',name: '쿠폰 이름'}, {id:'body',name:'쿠폰 내용'}, {id:'count',name:'현재까지 이용 수'}],
                body: [],
                config: {
                    search: true,
                    title: '삭제된 쿠폰',
                    sortBy: 'name',
                    sortOrder: 'desc',
                    search: 'false',
                    pagination: 'false',
                    toggle: false
                }
            }
        };
    }

    makeCoupon() {
        browserHistory.push('/admin/coupon_create');
    }

    render() {
        return (
            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="#">
                            <svg class="glyph stroked home">
                                <use xlinkHref="#stroked-home"></use>
                            </svg>
                        </a></li>
                        <li class="active">Icons</li>
                    </ol>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-primary btn-block" onClick={this.makeCoupon}>쿠폰 만들기
                        </button>
                    </div>
                </div>
                <CouponList data={this.state.couponList}/>
                <CouponList data={this.state.deletedCouponList}/>
                <div class="row">
                    <div class="col-lg-12">

                    </div>
                </div>
            </div>
        );
    }


}