import React from 'react';
import {browserHistory} from 'react-router'

export default class Contact extends React.Component {
    componentDidMount() {
    }

    render() {
        return (

            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <svg class="glyph stroked email">
                                    <use xlinkHref="#stroked-email"></use>
                                </svg>
                                Contact Form
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" action="" method="post">
                                    <fieldset>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="message">Your message</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" id="message" name="message"
                                                          placeholder="Please enter your message here..."
                                                          rows="5"></textarea>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12 widget-right">
                                                <button type="submit" class="btn btn-default btn-md pull-right">Submit
                                                </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>

                        <div class="panel panel-default chat">
                            <div class="panel-heading" id="accordion">
                                <svg class="glyph stroked two-messages">
                                    <use xlinkHref="#stroked-two-messages"></use>
                                </svg>
                                Chat
                            </div>

                            <div class="panel-body">
                                <ul>
                                    <li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="http://placehold.it/80/30a5ff/fff" alt="User Avatar" class="img-circle"/>
								</span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">John Doe</strong>
                                                <small class="text-muted">32 mins ago</small>
                                            </div>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante
                                                turpis, rutrum
                                                ut ullamcorper sed, dapibus ac nunc. Vivamus luctus convallis mauris, eu
                                                gravida
                                                tortor aliquam ultricies.
                                            </p>
                                        </div>
                                    </li>
                                    <li class="right clearfix">
								<span class="chat-img pull-right">
									<img src="http://placehold.it/80/dde0e6/5f6468" alt="User Avatar"
                                         class="img-circle"/>
								</span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="pull-left primary-font">Jane Doe</strong>
                                                <small class="text-muted">6 mins ago</small>
                                            </div>
                                            <p>
                                                Mauris dignissim porta enim, sed commodo sem blandit non. Ut scelerisque
                                                sapien eu
                                                mauris faucibus ultrices. Nulla ac odio nisl. Proin est metus, interdum
                                                scelerisque
                                                quam eu, eleifend pretium nunc. Suspendisse finibus auctor lectus, eu
                                                interdum
                                                sapien.
                                            </p>
                                        </div>
                                    </li>
                                    <li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="http://placehold.it/80/30a5ff/fff" alt="User Avatar" class="img-circle"/>
								</span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">John Doe</strong>
                                                <small class="text-muted">32 mins ago</small>
                                            </div>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante
                                                turpis, rutrum
                                                ut ullamcorper sed, dapibus ac nunc. Vivamus luctus convallis mauris, eu
                                                gravida
                                                tortor aliquam ultricies.
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="panel-footer">
                                <div class="input-group">
                                    <input id="btn-input" type="text" class="form-control input-md"
                                           placeholder="Type your message here..."/>
                                    <span class="input-group-btn">
								<button class="btn btn-success btn-md" id="btn-chat">Send</button>
							</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}