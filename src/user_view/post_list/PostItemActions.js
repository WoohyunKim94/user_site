export const CLICK_ITEM ='CLICK_ITEM'

export function clickItem(index){
    return {type: CLICK_ITEM, index}
}
