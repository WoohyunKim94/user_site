import {CLICK_ITEM} from './PostItemActions'

const initialState={
    clicks: []
}

export default function postList(state=initialState,action){
    switch (action.type){
        case CLICK_ITEM:
            return Object.assign({},state,{
                clicks: [...state.clicks,{
                    click: action.index,
                    created_at: Date.now()
                }]
            });
        default:
            return state;
    }
}

