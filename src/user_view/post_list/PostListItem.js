import React, {PropTypes} from 'react';
import {Link} from 'react-router'
import Image from '../_components/image/Image';
import styles from './PostListItem.css';
import CSSModules from 'react-css-modules';


class PostListItem extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(postId) {
        this.props.onClick(postId);
    }

    render() {
        return (
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" styleName="masonry-wrapper"
                 onClick={e => this.handleClick(this.props.postId)}>
                <div styleName="imageArea">
                    <Image data="food_pczqtj" width="300" height="210" opacity={true}/>
                    <div styleName="tags">
                        {
                            this.props.tags.map((tag, index)=> {
                                return (
                                    <div key={index}><span>{tag}</span></div>
                                );
                            })
                        }
                    </div>
                </div>
                <div styleName="bodyArea">
                    <span>이거슨 제목입니다. 우다다다다다다</span><br/>
                </div>
                <div styleName="footerArea">
                    <div styleName="share" class="share">
                        <li><a data-icon="facebook" href="#"></a></li>
                        <li><a data-icon="twitter" href="#"></a></li>
                        <li><a data-icon="patreon" href="#"></a></li>
                    </div>
                </div>
            </div>
        )
    }
}
PostListItem.propTypes = {
    onClick: PropTypes.func.isRequired,
    postId: PropTypes.number.isRequired,
    tags: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
};


module.exports = CSSModules(PostListItem, styles, {allowMultiple: true});
