import React from 'react';
import PostListItem from '../post_list/PostListItem';
import {clickItem} from './PostItemActions';
import Masonry from 'masonry-layout';
import {connect} from 'react-redux';
import CSSModules from 'react-css-modules';


class PostList extends React.Component {
    constructor() {
        super();
        if (window.innerWidth < 376) {
            this.columnWidth = window.innerWidth - 60;
        } else {
            this.columnWidth = 300;
        }
        this.state = {
            data: [],
            columnWidth: this.columnWidth
        };
        this.onPostItemClick = this.onPostItemClick.bind(this);
    }

    componentDidMount() {
        // fetch('/api/post/list?page=1').then((res)=> {
        //     res.json().then((dat)=> {
        //         this.setState({data: dat.posts});
        //         this.setState({hasMorePage: dat.hasMorePage});
        //     })
        // });
        // let msry = new Masonry('.masonry-container', {
        //     // options
        //     itemSelector: '.masonry-element',
        //     columnWidth: this.columnWidth,
        //     gutter: 10
        // });
    }

    onPostItemClick(postId) {
        this.props.dispatch(clickItem(postId));
        this.props.history.push('/post/3');
    }

    render() {
        return (
            <div class="container">
                <div class="row" style={{margin: '0 auto 5rem'}}>
                    {
                        [{id: 1}, {id: 2}, {id: 3}, {id: 4}].map(function (item, index) {
                            return <PostListItem key={index} tags={["#피자"]} postId={item.id}
                                                 onClick={this.onPostItemClick}/>;
                        }, this)
                    }
                </div>
                {/*{ this.state.hasMorePage ?*/}
                {/*<div class="post-loader">*/}
                {/*<div class="box">*/}
                {/*<div class="box-content">*/}
                {/*<div class="small-title post-loader-control"></div>*/}
                {/*</div>*/}

                {/*</div>*/}
                {/*</div>*/}
                {/*: '' }*/}
            </div>
        );
    }
}

function select(state) {
    return {};
}
export default connect(select)(PostList);