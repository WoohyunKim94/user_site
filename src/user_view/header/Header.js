import React from 'react';
import {Link} from 'react-router'
import Facebook from '../_components/facebook-login/Facebook';
import cookie from 'react-cookie';
import { connect } from 'react-redux';
import styles from './header.css'
import CSSModules from 'react-css-modules';

class Header extends React.Component {
    constructor(){
        super();
        this.state={logined:false, admin: false};
    }
    componentDidMount(){
        if(cookie.load("facebookId")){
            this.setState({logined:true});
        }
    }
    render() {
        return (
            <div id="wrapper">

                <header id="header">

                    <div class="container">

                        <div class="share">
                            <ul>
                                <li><a data-icon="facebook" href="#"></a></li>
                                {/*<li><a data-icon="twitter" href="#"></a></li>*/}
                                {/*<li><a data-icon="patreon" href="#"></a></li>*/}
                            </ul>
                        </div>
                        {/* // .share */}

                        <div class="main vertical-align-stack horizontal-align-right">

                            <div class="logo">
                                <Link to="home" title="Live">
                                    <img src="/res/logo.png"/>
                                </Link>
                            </div>

                            {/* // .logo */}

                            <nav class="light">
                                <ul>
                                    <li><Link to="/post">POST</Link></li>|
                                    <li><Link to="/about">ABOUT</Link></li>|
                                    {(!this.state.logined? <li><a href="#log-in-form">LOGIN</a></li> :<li><Link to="mypage">MY PAGE</Link></li>)}
                                </ul>
                            </nav>

                        </div>
                        {/* // .main */}

                    </div>
                    {/* // .container */}

                </header>
                <div id="log-in-form" class="modal">

                    <div class="modal-content">

                        <img src="/content/traveler/thumbs/logo-mini-212x55.png" width="212" height="55"
                             srcSet="/content/traveler/thumbs/logo-mini-212x55.png 212w, /content/traveler/thumbs/logo-mini-424x110.png 424w"
                             sizes="(max-width: 212px) 100vw, 212px" class="align-center" alt="Logo mini"/>
                        <Facebook />
                        {/*<form action="#filename" method="post">*/}

                            {/*<p><input type="email" placeholder="e-mail" class="width-full" required/></p>*/}
                            {/*<p><input type="password" placeholder="password" class="width-full" required/></p>*/}
                            {/*<p><input type="submit" class="width-full" value="Log in"/></p>*/}

                            {/*<p class="form-links">*/}
                                {/*<a href="#filename">Sign up &rsaquo;</a>*/}
                                {/*<a href="#filename">Forget password?</a>*/}
                            {/*</p>*/}

                        {/*</form>*/}

                    </div>

                    <a class="close"></a>

                </div>
            </div>
        )
    }
}
function select(state){
    return {
        clicks: state.clicks
    };
}
export default connect(select)(CSSModules(Header,styles));




