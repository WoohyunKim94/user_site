import React from 'react';
import ReactDOM from 'react-dom';
import {cloudinaryConfig, CloudinaryImage, CloudinaryVideo} from 'react-cloudinary';
import styles from './image.css';
import CSSModules from 'react-css-modules';

class Image extends React.Component {
    constructor() {
        super();
        cloudinaryConfig({
            cloud_name: 'seereal',
            api_key: '848649886355129',
            api_secret: 'YWi__2aPpepsOKv0myOwyGpE8mk'
        });
    }

    componentDidMount() {
        // console.log(ReactDOM.findDOMNode(this).parentNode.offsetWidth);
    }

    render() {
        return (<div styleName="img-wrapper">
                <div styleName={this.props.opacity ? "cover" : ""}></div>
                <CloudinaryImage style={this.props.style} class={this.props.classes} publicId={this.props.data}
                                 options={{width: this.props.width, height: this.props.height, crop: "thumb"}}/>

            </div>
        )
    }
}

module.exports = CSSModules(Image, styles, {allowMultiple: true});