'use strict';

import React from 'react';
import FacebookLogin from 'react-facebook-login';
import cookie from 'react-cookie';

class Facebook extends React.Component {
    constructor(props) {
        super(props);
    }

    responseFacebook(response){
        console.log(response);
        cookie.save('facebookId', response.id, { path: '/' });
        console.info({ facebookId: cookie.load('facebookId') });
        // "EAADC8Ox9UmkBAF0Xp69plOtApcEpdfPUelrdkK45KhiEBssqZCHh3QHDxcKlcvRsyhHPOCdfIiBglko1IpbKTZAqL9FEDNSgYp3BabUj3dHCv5VLbnUSY7jG0unw91nJoGH8UcymNq93mvQC0bLRiVsxhiNFIywM4ZBfypc2AZDZD"
        // email
        //     :
        //     "r45r54r45@naver.com"
        // expiresIn
        //     :
        //     6461
        // id
        //     :
        //     "1024123967649088"
        // name
        //     :
        //     "Woo Hyun Kim"
        // picture
        //     :
        //     Object
        // signedRequest
        //     :
        //     "9ntqAH79P3e0H2L2zbH0eQSUj-KdHgf10y5wq6FatRw.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUFnV3lFcHJ1dTlxQlU3Z0drT196Y3NZbUp4WC12SHZuYWpkVHNHMWwyN2VlNUJ2dGxLcklIX1E1T2ptYkwteGUyUW1OX1JwV0N3c2ViMkdIWnBuT0w2UWl4LUxldGNURXJCdTdaaFkyd3ZXM0VXaXdrNlNDTjl3ZlR2bzRLNTNrel9KbXkzMlVCYXRQYWc2OXIxUXktVXhwYVFQMWF3V050U1loa3FQZUZUa1FmMElhSk1QdDltTHc5SS1UUHV2bGFRejlsai1nSTNUY0pMQ0pVTG9oWGFYN3VSc2xmSFJPZTBJTmRNZ0RQOG5ZbXQzZEMxV09iNWdHb0dmWTlnRXRPSGQwTlBFZjV6NmJId2ItZ0QyZmpxdWlDajdROGxFVVVkOEFkazFFZWZHVV91MXBDYTA3VHZKRmNLVkZNZzhWV2tPb2xRaUNUWm5QbGN0ZkY3Z2NPMSIsImlzc3VlZF9hdCI6MTQ3MzQwMTUzOSwidXNlcl9pZCI6IjEwMjQxMjM5Njc2NDkwODgifQ"
        // userID
        //     :
        //     "1024123967649088"

    }

    render() {
        return (
            <FacebookLogin
                appId="214340015575657"
                autoLoad={false}
                fields="name,email,picture"
                callback={this.responseFacebook}
            />
        )
    }
}

export default Facebook;