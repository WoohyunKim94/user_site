import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (
            <footer id="footer">
                <div class="container">

                    <div class="end-note">

                        <div class="align-left">
                            <p>&copy; Seereal, 2016</p>
                        </div>

                        <div class="align-right">
                            <p><a href="/admin">Admin</a></p>
                        </div>

                        <a class="back" href="#wrapper" title="Back to top"></a>

                    </div>
                </div>
            </footer>
        )
    }
}






