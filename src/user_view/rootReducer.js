import {combineReducers} from 'redux'
import posts from './post/PostReducers';

const UserReducer = combineReducers({
    posts
});

export default UserReducer