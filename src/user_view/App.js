import React from 'react';
import UserHeader from './header/Header';
import UserFooter from './footer/Footer';


class App extends React.Component {
    constructor() {
        super();
        new Live();
    }

    render() {
        return (

            <div style={{position: 'relative',minHeight: '100vh'}}>
                <UserHeader/>
                {this.props.children}
                <UserFooter/>
            </div>

        )
    }
}

export default App;