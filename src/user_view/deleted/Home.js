import React from 'react';

var CommentList = React.createClass({
    getInitialState(){
        return {data: []};
    },
    componentDidMount: function () {
        new Live;
        // fetch('/api/admin').then((res)=> {
        //     res.json().then((dat)=> {
        //         console.log(dat);
        //         this.setState({data: dat});
        //     })
        // });
    },
    render: function () {
        return (<div>
            <div class="box-content">

                <h3 class="box-title"><span>{this.state.data.length} Comments</span></h3>

                <ul class="comments">
                    { this.state.data.map(function (item, index) {
                        return (

                            <li class="comment" key={index}>
                                <div class="body">
                                    <img src="/content/traveler/thumbs/avatar-2-64x64.jpg" width="64" height="64"
                                         srcSet="/content/traveler/thumbs/avatar-2-64x64.jpg 64w, /content/traveler/thumbs/avatar-2-128x128.jpg 128w"
                                         sizes="(max-width: 64px) 100vw, 64px" class="avatar" alt="Avatar 2"/>
                                    <cite class="author">{item.title} <span
                                        class="timestamp">{item.created_at}</span></cite>
                                    <div class="comment-content">
                                        <p>{item.body}</p>
                                    </div>
                                </div>
                            </li>

                        )
                    })
                    }

                </ul>
            </div>
        </div>);
    }
});
var CommentForm= React.createClass({
    getInitialState(){
        return {input:''};
    },
    handleInput(e){
        console.log(this.state.input);
        this.setState({input:''});
        e.preventDefault();
    },
    handleChange(e){
        this.setState({input:e.target.value});
    },
    render(){
        return (
          <form>
              <input type="text" value={this.state.input} onChange={this.handleChange}/>
              <button onClick={this.handleInput}>전송</button>
          </form>
        );
    }
});
export default class Home extends React.Component {
    componentDidMount() {

    }

    render() {
        return (
            <div>
                <div class="container">
                    <CommentForm/>
                    <section id="comments" class="box-comments">


                        <CommentList/>

                    </section>
                </div>
            </div>
        );
    }
}