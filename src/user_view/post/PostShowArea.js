import React from 'react'
import PostShow from './PostShow'

export default class PostShowArea extends React.Component{
    componentDidMount(){
        let naver=window.naver.maps;
        let map = new naver.Map('map', {
            center: new naver.LatLng(37.3595704, 127.105399),
            zoom: 50
        });
        let marker = new naver.Marker({
            position: new naver.LatLng(37.3595704, 127.105399),
            map: map
        });
    }
    render(){
        return (
            <article class="box-page">

                <div class="box-alpha">
                    <div class="featured-video embed">
                        <iframe
                            src="https://player.vimeo.com/video/29836061?color=5A88C4&title=0&byline=0&portrait=0"
                            width="500" height="281"
                            allowFullScreen></iframe>
                    </div>

                </div>

                <div class="box-beta">
                    <div class="box-content" style={{margin: 0}}>

                        <h2 class="box-title">{this.props.show.title}</h2>
                        <p class="small-title"><span>{this.props.show.created_at}</span></p>

                        <PostShow data={this.props.show.body}/>

                        {/*<div class="share">*/}
                            {/*<span>Share:</span>*/}
                            {/*<ul>*/}
                                {/*<li><a data-icon="facebook" href="#"></a></li>*/}
                                {/*<li><a data-icon="twitter" href="#"></a></li>*/}
                                {/*<li><a data-icon="googleplus" href="#"></a></li>*/}
                                {/*<li><a data-icon="pinterest" href="#"></a></li>*/}
                                {/*<li><a data-icon="linkedin" href="#"></a></li>*/}
                                {/*<li><a data-icon="stumbleupon" href="#"></a></li>*/}
                            {/*</ul>*/}
                        {/*</div>*/}
                        <div id="map" style={{width:'80%',margin: 'auto',height:'300px'}}></div>

                    </div>

                    {/*<ul class="tags">*/}
                        {/*<li><a href="traveler-index.html"><span*/}
                            {/*data-hover="north america">north america</span></a></li>*/}
                        {/*<li><a href="traveler-index.html"><span data-hover="first tag">first tag</span></a>*/}
                        {/*</li>*/}
                        {/*<li><a href="traveler-index.html"><span data-hover="tag two">tag two</span></a></li>*/}
                        {/*<li><a href="traveler-index.html"><span data-hover="video">video</span></a></li>*/}
                        {/*<li><a href="traveler-index.html"><span data-hover="relation">relation</span></a>*/}
                        {/*</li>*/}
                    {/*</ul>*/}

                </div>

            </article>
        )
    }
}