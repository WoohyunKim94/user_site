import React from 'react';
import Image from '../_components/image/Image';
import CSSModules from 'react-css-modules';
import styles from './PostCoupon.css';
import CouponBean from '../bean/CouponBean';

class PostCoupon extends React.Component {
    constructor() {
        super();
        this.state = {
            // opened: false,
            // dateOpened: false,
            // timeOpened: false,
            count: 1,
            // date: '',
            // time: '',
            phoneCheck: false
        };
        this.coupon = new CouponBean();
        this.coupon.setUserCount(1);
        this.countPlus = this.countPlus.bind(this);
        this.countMinus = this.countMinus.bind(this);
        // this.toggleDate = this.toggleDate.bind(this);
        // this.toggleTime = this.toggleTime.bind(this);
        // this.selectDate = this.selectDate.bind(this);
        // this.selectTime = this.selectTime.bind(this);
        this.setPhone = this.setPhone.bind(this);
        this.reserve = this.reserve.bind(this);
    }

    countPlus() {
        this.setState({
            count: this.state.count + 1
        }, ()=> {
            this.coupon.setUserCount(this.state.count);
        });
    }

    countMinus() {
        if (this.state.count != 1) {
            this.setState({
                count: this.state.count - 1
            }, ()=> {
                this.coupon.setUserCount(this.state.count);
            });
        }
    }

    // toggleDate() {
    //     this.setState({
    //         dateOpened: !this.state.dateOpened,
    //         timeOpened: false
    //     });
    // }
    //
    // toggleTime() {
    //     this.setState({
    //         timeOpened: !this.state.timeOpened,
    //         dateOpened: false
    //     });
    // }
    //
    // selectDate(obj) {
    //     this.toggleDate();
    //     this.setState({
    //         date: obj.public_date
    //     });
    //     this.coupon.setDate(obj);
    // }
    //
    // selectTime(obj) {
    //     this.toggleTime();
    //     this.setState({
    //         time: obj.public_time
    //     });
    //     this.coupon.setTime(obj);
    // }

    setPhone(event) {
        if (event.target.value.length == 11) {
            this.setState({
                phoneCheck: true
            });
            this.coupon.setPhone(event.target.value);
        } else {
            this.setState({
                phoneCheck: false
            });
        }
    }

    reserve() {
        if (!this.coupon.validate()) {
            alert('폼을 완성해주세요');
        } else {
            this.props.onSubmit(this.coupon);
        }
    }

    render() {
        let isReserved = this.props.isReserved;
        return (
            <div >
                <div styleName="imageArea" onClick={this.props.onClick}>
                    <Image data="food_pczqtj" width="300" height="250" opacity={true}/>
                    <div styleName="textArea">
                        <div styleName="inlineAreaLeft">
                            <div styleName="bigText">
                                <span styleName="fontLight">패밀리 리프 립스 세트</span>
                            </div>
                            <div styleName="smallText">
                                <span styleName="fontLight">#스테이크 #빕스 #헬로우</span>
                            </div>
                        </div>
                        <div styleName="inlineAreaRight">
                            <div styleName="smallText">
                            </div>
                            <div styleName="bigText">
                                <span styleName="fontBold">26,000원</span>
                            </div>
                        </div>
                    </div>
                    {/*<div styleName="upperFlag">*/}
                    {/*<span styleName="percentNum">54</span><span styleName="percent">%</span>*/}
                    {/*</div>*/}
                    {/*<div styleName="bottomFlag">*/}
                    {/*</div>*/}
                </div>
                <div
                    styleName={(this.props.isOpened ? "open-area formArea" : "closed-area formArea")}>
                    {/*<DateTimePicker  time={false} format="YYYY-MM-DD"/>*/}
                    {/*<div styleName="datetime" onClick={this.toggleDate}>*/}
                    {/*<input type="text" disabled="true" placeholder="날짜를 선택하세요" value={this.state.date}/>*/}
                    {/*<div styleName="clickbutton"></div>*/}
                    {/*</div>*/}
                    {/*<div styleName={(this.state.dateOpened ? "open-area selectArea" : "closed-area selectArea")}>*/}
                    {/*/!*사용가능날짜를 전부 prop으로 받아옴*!/*/}
                    {/*/!*가게 주인은 전부 클릭해서 날짜를 놓음*!/*/}
                    {/*{*/}
                    {/*[{public_date: '9월 7일 (화)'}, {public_date: '9월 8일 (수)'}, {public_date: '9월 9일 (목)'}, {public_date: '9월 10일 (금)'}].map(function (item, key) {*/}
                    {/*return (*/}
                    {/*<div styleName="selectList" onClick={this.selectDate.bind(this, item)}*/}
                    {/*data={item} key={key}>*/}
                    {/*<span styleName="left">*/}
                    {/*{item.public_date}*/}
                    {/*</span>*/}
                    {/*<span styleName="right">*/}
                    {/*32개*/}
                    {/*</span>*/}
                    {/*</div>*/}
                    {/*);*/}
                    {/*}, this)}*/}
                    {/*</div>*/}
                    {/*<div styleName="datetime" onClick={this.toggleTime}>*/}
                    {/*<input type="text" disabled="true" placeholder="시간을 선택하세요" value={this.state.time}/>*/}
                    {/*<div styleName="clickbutton"></div>*/}
                    {/*</div>*/}
                    {/*<div styleName={(this.state.timeOpened ? "open-area selectArea" : "closed-area selectArea")}>*/}
                    {/*/!*사용가능날짜를 전부 prop으로 받아옴*!/*/}
                    {/*/!*가게 주인은 전부 클릭해서 날짜를 놓음*!/*/}
                    {/*{[{public_time: '11시 30분'}, {public_time: '11시 31분'}, {public_time: '11시 32분'}, {public_time: '11시 33분'}].map(function (item, index) {*/}
                    {/*return (*/}
                    {/*<div styleName="selectList" onClick={this.selectTime.bind(this, item)} key={index}>*/}
                    {/*<span styleName="left">*/}
                    {/*{item.public_time}*/}
                    {/*</span>*/}
                    {/*</div>*/}
                    {/*);*/}
                    {/*}, this)}*/}
                    {/*</div>*/}
                    <div styleName="description">
                        <ul>
                            <li>음식에 대한 간략한 업주의 설명음식에 대한 간략한 업주의 설명음식에 대한 간략한 업주의 설명음식에 대한 간략한 업주의 설명</li>
                        </ul>
                    </div>
                    <div styleName="datetime">
                        <div styleName="minus" onClick={this.countMinus}></div>
                        <div styleName="number">{this.state.count}명</div>
                        <div styleName="plus" onClick={this.countPlus}></div>
                    </div>
                    <div styleName="phone">
                        <input type="text" onChange={this.setPhone} maxLength="11" minLength="11"
                               placeholder="전화번호를 입력해 주세요 (- 제외)"/>
                        <div styleName={this.state.phoneCheck ? "phone-check" : ''}></div>
                    </div>
                    <div styleName="datetime submit-btn">
                        <button onClick={this.reserve}>예약하기</button>
                    </div>
                </div>
                <div
                    styleName={(isReserved ? "open-area formArea" : "closed-area formArea")}>
                    <div styleName="datetime submit-btn no-margin">
                        <button>이미 예약한 곳이 있어요</button>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = CSSModules(PostCoupon, styles, {allowMultiple: true});