import React from 'react';
import Image from '../_components/image/Image';

class PostComment extends React.Component{
    render(){
        return (
            <li class="comment">
                <div class="body">
                    <Image classes="avatar" data="v1vllihm9okoa3whfjp8" width="64" height="64"/>
                    <cite class="author">김우현<span class="timestamp">4시간 전</span></cite>
                    <div class="comment-content">
                        <p>댓글입니다</p>
                    </div>

                </div>
            </li>
        );
    }
}

module.exports = PostComment;