import fetch from 'isomorphic-fetch'

export const SELECT_COUPON = 'SELECT_COUPON'
export function selectCoupon(postId, couponId) {
    return {
        type: SELECT_COUPON,
        couponId,
        postId
    }
}
export const SUBMIT_COUPON = 'SUBMIT_COUPON'
export function submitCoupon(postId, couponId) {
    return {
        type: SUBMIT_COUPON,
        couponId,
        postId
    }
}
//coupon fetch
export const COUPON_FETCH_START = 'COUPON_FETCH_START';
export const COUPON_FETCH_SUCCESS = 'COUPON_FETCH_SUCCESS'
function couponFetchStart(postId){
    return {
        type: COUPON_FETCH_START,
        postId
    }
}
function couponFetchSuccess(postId, response){
    return {
        type: COUPON_FETCH_SUCCESS,
        postId,
        response
    }
}
function couponFetch(postId){
    return function (dispatch){
        dispatch(couponFetchStart(postId));
        return fetch('')
            .then(response => response.json())
            .then(json=> dispatch(couponFetchSuccess(postId,json)));
    }
}
function shouldFetchCoupon(state,postId){
    console.log("should fetch coupon?");
    if(state.posts[postId].coupon.couponList.length!=0){
        return false;
    }else if(state.posts[postId].coupon.isFetching){
        return false;
    }else{
        return true;
    }
}
export function fetchCouponIfNeeded(postId){
    return (dispatch, getState) => {
        if (shouldFetchCoupon(getState(), postId)) {
            return dispatch(couponFetch(postId));
        } else {
            return Promise.resolve();
        }
    }
}

//post fetch

export const POST_FETCH_START = 'POST_FETCH_START'
export const POST_FETCH_SUCCESS = 'POST_FETCH_SUCCESS'
export const POST_FETCH_FAIL = 'POST_FETCH_FAIL'

function postFetchStart(postId) {
    return {
        type: POST_FETCH_START,
        postId
    }
}
function postFetchSuccess(postId, response) {
    return {
        type: POST_FETCH_SUCCESS,
        postId,
        response
    }
}
function postFetchFail(postId, error) {
    return {
        type: POST_FETCH_FAIL,
        postId,
        error
    }
}
function shouldFetchPost(state, postId) {
    if (!state.posts[postId]) {
        // should fetch
        return true;
    } else if (state.posts[postId].isFetching) {
        // 다운받고 있을 때
        return false;
    } else {
        // 이미 있을 때
        return false;
    }
}
function postFetch(postId) {
    return function (dispatch) {
        dispatch(postFetchStart(postId));
        return fetch('/api/post/open?id=' + postId)
            .then(response => response.json())
            .then(json => dispatch(postFetchSuccess(postId, json)))
        // .catch(error => dispatch(postFetchFail(postId, error)))
    }
}

export function fetchPostIfNeeded(postId) {
    return (dispatch, getState) => {
        if (shouldFetchPost(getState(), postId)) {
            return dispatch(postFetch(postId));
        } else {
            return Promise.resolve();
        }
    }
}

