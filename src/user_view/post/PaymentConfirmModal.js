import React from 'react';
import Modal from 'react-responsive-modal'
import styles from './PaymentConfirmModal.css'
import CSSModules from 'react-css-modules';

class PaymentConfirmModal extends React.Component {
    constructor(){
        super();
        this.onConfirm = this.onConfirm.bind(this);
        this.onDeny = this.onDeny.bind(this);
    }
    onConfirm(){
        this.props.paymentModal(true);
    }
    onDeny(){
        this.props.paymentModal(false);
    }
    render(){
        return (
            <Modal open={this.props.openPaymentConfirmModal} onClose={this.onDeny} modalStyle={{minWidth:'300px'}} little>
                <h2>거래명세서</h2>
                <div styleName="payment-info-row">
                    <div styleName="payment-info-col">
                        메뉴 이름
                    </div>
                    <div styleName="payment-info-col">
                        이재하의 떡볶이
                    </div>
                </div>
                <div styleName="payment-info-row">
                    <div styleName="payment-info-col">
                        메뉴 수량
                    </div>
                    <div styleName="payment-info-col">
                        13개
                    </div>
                </div>
                <div styleName="payment-info-row">
                    <div styleName="payment-info-col">
                        총 가격
                    </div>
                    <div styleName="payment-info-col">
                        340000원
                    </div>
                </div>
                <div styleName="payment-info-row">
                    <div styleName="payment-info-col">
                        이용약관1
                    </div>
                    <div styleName="payment-info-col">
                        <input type="checkbox"/>
                    </div>
                </div>
                <div styleName="payment-info-row">
                    <div styleName="payment-info-col">
                        이용약관2
                    </div>
                    <div styleName="payment-info-col">
                        <input type="checkbox"/>
                    </div>
                </div>
                <button onClick={this.onConfirm}>구입하기</button>
                <button onClick={this.onDeny}>뒤로가기</button>
            </Modal>
        )
    }
}
export default CSSModules(PaymentConfirmModal,styles);


