import React from 'react';
import PostAuthor from './PostAuthor';
import PostsSeeMore from './PostsSeeMore';
import { connect } from 'react-redux';
import {selectCoupon,fetchPostIfNeeded, fetchCouponIfNeeded} from './PostActions'
import PostShowArea from './PostShowArea'
import CommentList from './CommentList'
import CouponList from './CouponList'
import CSSModules from 'react-css-modules';
import styles from './Post.css';
import PaymentConfirmModal from './PaymentConfirmModal';

class Post extends React.Component {
    constructor() {
        super();
        this.state={
            openModal: false,
            modalInfo: {}
        };
        this.couponSubmit=this.couponSubmit.bind(this);
        this.paymentModal=this.paymentModal.bind(this);
    }

    componentDidMount() {
        this.postId=this.props.params.id;
        const {dispatch, currentCoupon, show} = this.props;
        dispatch(fetchPostIfNeeded(this.props.params.id));
        dispatch(fetchCouponIfNeeded(this.props.params.id));
    }
    paymentModal(confirm){
        if(confirm){
            this.setState({
                openModal: false
            });
            let IMP = window['IMP'];
            IMP.init('iamport');
            IMP.request_pay({
                pg : 'html5_inicis',
                pay_method : 'card',
                merchant_uid : 'merchant_' + new Date().getTime(),
                name : '주문명:결제테스트',
                amount : 14000,
                buyer_email : 'iamport@siot.do',
                buyer_name : '구매자이름',
                buyer_tel : '010-1234-5678',
                buyer_addr : '서울특별시 강남구 삼성동',
                buyer_postcode : '123-456'
            }, function(rsp) {
                if ( rsp.success ) {
                    var msg = '결제가 완료되었습니다.';
                    msg += '고유ID : ' + rsp.imp_uid;
                    msg += '상점 거래ID : ' + rsp.merchant_uid;
                    msg += '결제 금액 : ' + rsp.paid_amount;
                    msg += '카드 승인번호 : ' + rsp.apply_num;
                } else {
                    var msg = '결제에 실패하였습니다.';
                    msg += '에러내용 : ' + rsp.error_msg;
                }

                alert(msg);
            });

        }else{
            this.setState({
                openModal: false
            });
            //결제 안함
        }
    }
    couponSubmit(coupon){
        console.log(coupon);
        this.setState({
            openModal: true,
            modalInfo: {
                phone: coupon.phone,
                user_count: coupon.user_count
                //TODO 추가적으로 쿠폰의 정보 들어가야함.
            }
        });
    }
    render() {
        const {dispatch, currentCoupon, show} = this.props;
        return (
            <div class="container">
                <div class="holder sidebar-right">
                    <div class="holder-content">
                        <PostShowArea show={show}/>
                    </div>
                    <aside class="holder-sidebar">
                        <CouponList onSubmit={this.couponSubmit} isReserved={false} coupons={[{id:10}]} currentCoupon={currentCoupon} onClick={couponId=>dispatch(selectCoupon(this.postId,couponId))}/>
                    </aside>
                </div>
                <PostAuthor/>
                <CommentList comments={[1, 2, 3]}/>
                <PostsSeeMore/>
                <PaymentConfirmModal openPaymentConfirmModal={this.state.openModal} paymentModal={this.paymentModal}/>
            </div>
        )
    }
}

function select(state,instance){
    //여기서 state 는 store 전체를 가져온다.
    return {
        currentCoupon: state.posts[instance.params.id]?state.posts[instance.params.id].coupon.currentCoupon:null,
        show: state.posts[instance.params.id]?state.posts[instance.params.id].show:{title:'',body:'',created_at:''}
    }
}


export default connect(select)(CSSModules(Post,styles));