import React from 'react';
import PostCoupon from './PostCoupon';

export default class CouponList extends React.Component {
    render() {
        return (
            <div class="widget">
                <h3 class="widget-title"><span>히든 메뉴</span></h3>
                <div class="posts-list">
                    <ul>
                        {
                            this.props.coupons.map(function (coupon, index) {
                                return (
                                    <li>
                                        <PostCoupon key={coupon.id}
                                                    onSubmit={this.props.onSubmit}
                                                    isOpened={(this.props.currentCoupon == coupon.id ? true : false)}
                                                    onClick={this.props.onClick.bind(this, coupon.id)} isReserved={this.props.isReserved}/>
                                    </li>
                                );
                            }, this)
                        }
                    </ul>
                </div>
            </div>
        );
    }
}