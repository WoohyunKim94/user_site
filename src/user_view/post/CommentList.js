import React from 'react';
import PostComment from './PostComment';

export default class CommentList extends React.Component {
    render() {
        return (
            <section id="comments" class="box-comments">

                <div class="box-content">

                    <h3 class="box-title"><span>4 Comments</span></h3>

                    <ul class="comments">
                        {this.props.comments.map(function (comment, index) {
                            return (
                                <PostComment key={index}/>
                            );
                        })}
                    </ul>
                </div>
            </section>
        );
    }

}