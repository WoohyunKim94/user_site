import React from 'react';
import Quill from 'quill';

class PostShow extends React.Component{
    constructor(){
        super();
        this.editor=Object;
    }
    componentDidMount() {
        var toolbarOptions = [];
        var options = {
            modules: {
                toolbar: false
            },
            readOnly: true,
            theme: 'snow'
        };
        this.editor = new Quill('.showPostArea', options);

        //여기서는 이미지가 들어갈 곳의 parent 의 사이즈를 알 수가 있음.
        //해당 사이즈에 맞춰서 url 만들고 끼워 넣어주면됨.
        // this.props.imageList; - 이거 받을 필요도 없어


        // 이거를 받아서 이미지들의 주소를 현재 뷰포트 사이즈에 맞춰서 요청해야함.
        // max 랑 비교해서 더 작으면 요청하기.
        //만약에 max가 내가 원하는 사이즈보다 작으면 그냥 해당 이미지 url 쓰면됨.
        // 사실상 db랑 왔다갔다하거나 서버랑 왔다갔다 할 필요도 없음. 단지 url parameter 만 만들어서 뒤에 붙여주면됨.

        //TODO 이미지 위치에 placeholder 넣어놓기
        //그러려면 우선 width를 100 으로 잡아놓고 해당 자리 안에 svg 를 넣어놓으면 될듯.
        //높이 값을 어떻게 할
        //지금은 src자리를 빈칸으로 놔두었음.
        var length=$(".showPostArea .uploaded_image").length;
        for(var i=0; i<length; i++){
            var item=$(".showPostArea .uploaded_image")[i];
            var desiredWidth=item.parentNode.offsetWidth;지
            if(desiredWidth>item.getAttribute('maxwidth')){
                item.style['max-width']=item.getAttribute('maxwidth')+"px";
                item.setAttribute('src','http://res.cloudinary.com/seereal/image/upload'+item.getAttribute('srcpart'));
            }else{
                item.style['max-width']=desiredWidth+"px";
                item.setAttribute('src','http://res.cloudinary.com/seereal/image/upload/'+'c_scale,q_100,w_'+desiredWidth+item.getAttribute('srcpart'));
            }
            item.style['width']='100%';
        }
    }
    componentDidUpdate(){
        var length=$(".showPostArea .uploaded_image").length;
        for(var i=0; i<length; i++){
            var item=$(".showPostArea .uploaded_image")[i];
            var desiredWidth=item.parentNode.offsetWidth;
            if(desiredWidth>item.getAttribute('maxwidth')){
                item.style['max-width']=item.getAttribute('maxwidth')+"px";
                item.setAttribute('src','http://res.cloudinary.com/seereal/image/upload'+item.getAttribute('srcpart'));
            }else{
                item.style['max-width']=desiredWidth+"px";
                item.setAttribute('src','http://res.cloudinary.com/seereal/image/upload/'+'c_scale,q_100,w_'+desiredWidth+item.getAttribute('srcpart'));
            }
            item.style['width']='100%';
        }
    }
    render(){
        return (
            <div class="showPostArea" style={{border:'none'}} dangerouslySetInnerHTML={{__html:'<div class="ql-editor" contenteditable="false">'+this.props.data+'</div>'}}></div>
        )
    }

}
module.exports=PostShow;