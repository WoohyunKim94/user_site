import React from 'react';
import Image from '../_components/image/Image';
import {Link} from 'react-router'

class PostsSeeMore extends React.Component {

    render() {
        let current=this.props.currentPostId;
        let prev=current-1
        return (
            <div class="box">

                <div class="box-content">

                    <div class="navigation">
                        <div>
                            <Link class="prev" to="">
                                <span class="nav-link"><span>&lsaquo; 이전 포스트</span></span>
                                <Image data="v1vllihm9okoa3whfjp8" width="560" height="315"/>
                                <span class="post-title">신촌 먹자 골목 맛집 맛진에 가다!</span>
                            </Link>
                            <Link class="next" to="">
                                <span class="nav-link"><span>다음 포스트 &rsaquo;</span></span>
                                <Image data="v1vllihm9okoa3whfjp8" width="560" height="315"/>
                                <span class="post-title">강남 카페 뒷골목 커피숍에 가다!</span>
                            </Link>
                        </div>
                    </div>

                </div>

            </div>
        );
    }
}
module.exports = PostsSeeMore;