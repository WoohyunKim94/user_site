import {
    SELECT_COUPON,
    SUBMIT_COUPON,
    COUPON_FETCH_START,
    COUPON_FETCH_SUCCESS,
    POST_FETCH_START,
    POST_FETCH_SUCCESS,
    POST_FETCH_FAIL,
} from './PostActions';


function coupon(state, action) {
    // post 객체안의 coupon 객체를 받아야한다.
    switch (action.type) {
        case SELECT_COUPON:
            if(state.currentCoupon==action.couponId){
                //열려있던 쿠폰이면 닫아준다.
                return Object.assign({}, state, {
                    currentCoupon: null
                });
            }else{
                return Object.assign({}, state, {
                    currentCoupon: action.couponId
                });
            }
        case SUBMIT_COUPON:
            //ajax
            return state;
        case COUPON_FETCH_START:
            return Object.assign({},state,{
                isFetching: true
            });
        case COUPON_FETCH_SUCCESS:
            return Object.assign({},state,{
                couponList: action.response
            });
        default:
            return state;
    }
}
function post(state={
    coupon:{
        currentCoupon: null,
        couponList: [],
        isFetching: false
    },
    show:{
        title:'',
        body:'',
        created_at:''
    },
    isFetching:false
},action){
    // state로 단일 포스트 객체가 들어옴
    switch (action.type){
        case POST_FETCH_START:
            return Object.assign({},state,{
                    isFetching: true
            });
        case POST_FETCH_SUCCESS:
            return Object.assign({},state,{
                    isFetching: false,
                    show: {
                        title: action.response.title,
                        body: action.response.body,
                        created_at: action.response.created_at
                    }
            });
        default:
            return state;
    }
}
export default function posts(state={}, action) {
    // 여기서 state 로 들어오는 것은 store 안의 posts 객체임
    switch (action.type) {
        case COUPON_FETCH_START:
        case COUPON_FETCH_SUCCESS:
        case SELECT_COUPON:
            //포스트s array 안에서 index 찾기
            return Object.assign({}, state,{
                    [action.postId]: Object.assign({},state[action.postId],{
                        coupon: coupon(state[action.postId].coupon, action)
                    })
                })
        case POST_FETCH_START:
        case POST_FETCH_SUCCESS:
        case POST_FETCH_FAIL:
            //posts 객체 안에서
            return Object.assign({},state, {
                [action.postId]: post(state[action.postId],action)
            });
        default:
            return state;
    }
}
