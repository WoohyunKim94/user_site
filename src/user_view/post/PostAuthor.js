import React from 'react';
import Image from '../_components/image/Image';

class PostAuthor extends React.Component{
    render(){
        return (
            <section class="box-author">

                <figure class="featured-image">
                    <Image data="v1vllihm9okoa3whfjp8" width="320" height="320"/>
                </figure>

                <div class="box-content">
                    {/*<div class="share featured">*/}
                        {/*<span>Find me at:</span>*/}
                        {/*<ul>*/}
                            {/*<li><a data-icon="skype" href="#"></a></li>*/}
                            {/*<li><a data-icon="dribbble" href="#"></a></li>*/}
                            {/*<li><a data-icon="vine" href="#"></a></li>*/}
                            {/*<li><a data-icon="soundcloud" href="#"></a></li>*/}
                        {/*</ul>*/}
                    {/*</div>*/}
                    {/* // .share */}

                    <h2 class="box-title">코이코이의 한마디</h2>

                    <p>저리가 저리가</p>

                </div>
                {/* // .box-content */}

            </section>
        );
    }
}

module.exports=PostAuthor;