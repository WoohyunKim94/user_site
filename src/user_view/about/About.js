import React from 'react';
export default class About extends React.Component {
    render() {
        return (
            <div class="container">

                <article class="box-page">

                    <div class="box-beta">

                        <div class="box-content">

                            <h5 class="line"><span>My main sponsors</span></h5>

                            <div class="flex-columns">

                                <div class="row">

                                    <div class="cell-1-2">

                                        <img src="content/traveler/thumbs/sponsor-logo-09-385x200.png" width="385" height="200" srcset="content/traveler/thumbs/sponsor-logo-09-385x200.png 385w, content/traveler/thumbs/sponsor-logo-09-770x400.png 770w" sizes="(max-width: 385px) 100vw, 385px" class="align-center" alt="Sponsor logo 09" />

                                        <p>Mattis felis non a lobortis leo risus augue sociis nibh. Lacusiner feug euismod nisl pharetra mattis eiusmod esse nisi at. Pulvinar cupidatat cubilia netus aliqua cras tempus class. Volutpat massa donec sunt rhoncus.</p>

                                    </div>

                                    <div class="cell-1-2">

                                        <img src="content/traveler/thumbs/sponsor-logo-10-385x200.png" width="385" height="200" srcset="content/traveler/thumbs/sponsor-logo-10-385x200.png 385w, content/traveler/thumbs/sponsor-logo-10-770x400.png 770w" sizes="(max-width: 385px) 100vw, 385px" class="align-center" alt="Sponsor logo 10" />

                                        <p>Voluptate vel aliquip metus eros rutrum himenaeos. Cursus et dui facilisis nisl  nascetur quisque. dolore venenatis netus ullamco hendrerit ad fusce aliqua erat. Risus suspendisse <a href="#example-hyperlink">example hyperlink</a> veniam lacus.</p>

                                    </div>

                                </div>{/* // .row */}

                            </div>{/* // .flex-columns */}

                            <h5 class="line"><span>Additional sponsors</span></h5>

                            <div class="gallery" data-gallery-columns="4">

                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-01-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-01-224x90.png 224w, content/traveler/thumbs/sponsor-logo-01-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 01" />
                                </a>
                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-02-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-02-224x90.png 224w, content/traveler/thumbs/sponsor-logo-02-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 02" />
                                </a>
                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-03-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-03-224x90.png 224w, content/traveler/thumbs/sponsor-logo-03-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 03" />
                                </a>
                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-04-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-04-224x90.png 224w, content/traveler/thumbs/sponsor-logo-04-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 04" />
                                </a>
                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-05-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-05-224x90.png 224w, content/traveler/thumbs/sponsor-logo-05-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 05" />
                                </a>
                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-06-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-06-224x90.png 224w, content/traveler/thumbs/sponsor-logo-06-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 06" />
                                </a>
                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-07-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-07-224x90.png 224w, content/traveler/thumbs/sponsor-logo-07-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 07" />
                                </a>
                                <a href="traveler-index.html" class="hover-grayscale">
                                    <img src="content/traveler/thumbs/sponsor-logo-08-224x90.png" width="224" height="90" srcset="content/traveler/thumbs/sponsor-logo-08-224x90.png 224w, content/traveler/thumbs/sponsor-logo-08-448x180.png 448w" sizes="(max-width: 224px) 100vw, 224px" alt="Sponsor logo 08" />
                                </a>

                            </div>{/* // .gallery */}

                            <h5 class="line"><span>Special thanks</span></h5>

                            <div class="text-columns" data-text-columns-count="4">

                                Aksel Henriksen<br />
                                Albert Lassen<br />
                                Alma P. Fisher<br />
                                Arlena Bech<br />
                                Artus Moreau<br />
                                Brice Quessy<br />
                                Corette Chassé<br />
                                Elvisa Perea Rael<br />
                                Gabrielle Lazure<br />
                                Gregory K. Smith<br />
                                Guillaume Douffet<br />
                                Jacob Hinton<br />
                                James Hardin<br />
                                Johan Jessen<br />
                                Jules Beaulé<br />
                                Karlotta Berthiaume<br />
                                Kellie Findley<br />
                                Laure Lagueux<br />
                                Lea J. Madsen<br />
                                Line M. Dam<br />
                                Macra Fuentes Luna<br />
                                Mallory Fontaine<br />
                                Marcus Mortensen<br />
                                Minette Marleau<br />
                                Noah Kristiansen<br />
                                Patrick Mortensen<br />
                                Rabican Lafrenière<br />
                                Rachel McCurdy<br />
                                Richard Wetmore<br />
                                Roy J. Comacho<br />
                                Sofia Larsen<br />
                                Thea Mathiesen<br />
                                Tilde G. Andresen<br />
                                Tomé Vigil Ávalos<br />
                                Yudit Castillo Razo

                            </div>{/* // .text-columns */}

                            <hr />

                            <p>Thanks you for significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt. If you'd like to support me too, please <a href="traveler-contact.html">contact me</a>.</p>

                        </div>{/* // .box-content */}

                    </div>{/* // .box-beta */}

                </article>{/* // .box-page */}

            </div>
        );
    }
}
