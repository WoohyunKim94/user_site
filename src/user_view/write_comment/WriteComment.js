import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './WriteComment.css'
import Image from '../_components/image/Image'
import { Router } from 'react-router';

class WriteComment extends React.Component {
    static get contextTypes() {
        return {
            router: React.PropTypes.object.isRequired,
        };
    }
    constructor() {
        super();
        this.state = {
            image: null
        }
    }
    componentWillMount(){
        let user=this.props.location.query.user;
        if(user===undefined){
            alert('접근 권한이 없습니다.');
            this.context.router.push('/');
        }
    }
    render() {
        return (
            <div styleName="wrapper">
                <div>
                    <Image data="food_pczqtj" width="300" height="250" opacity={true}/>
                    맛있는 공익스타일 립스테이크<br/>
                </div>
                <div>
                    맛있게 이용하셨나요? 댓글과 사진으로 다른 사람들과 경험을 공유해보세요~
                </div>
                <img src={this.state.image}/>

                <div>
                    <textarea placeholder="서로 아름다운 말만 해주었으면 좋겠어요~" required></textarea>
                    <button>이미지 업로드</button>
                </div>
                <br/>
                <div>
                    <button>댓글 올리기</button>
                </div>
            </div>
        )
    }
}
export default CSSModules(WriteComment, styles);