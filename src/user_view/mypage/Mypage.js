import React from 'react';
import {Router, Route, Link, browserHistory, IndexRoute} from 'react-router'
import PostCoupon from '../post/PostCoupon';

var CouponList = React.createClass({
    render: function () {
        return (<div >
                { this.props.coupons.map(function (item) {
                    return (
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <PostCoupon/>
                            <div style={{background: 'white'}}>
                                <div>장소를 보여주는 지도가 들어가요</div>

                            </div>
                        </div>
                    );
                })
                }
            </div>
        )
    }
});

export default class Mypage extends React.Component {


    render() {
        let data = ['red', 'green', 'blue'];
        return (
            <div class="container">
                <div class="row">
                    <CouponList coupons={ data }/>
                </div>
            </div>
        );
    }
}