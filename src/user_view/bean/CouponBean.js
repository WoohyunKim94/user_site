class CouponBean {
    constructor(){
        this.date=undefined;
        this.time=undefined;
        this.user_count=undefined;
        this.phone=undefined;
    }
    setUserCount(count){
        this.user_count=count;
    }
    // setDate(date){
    //     this.date=date;
    // }
    // setTime(time){
    //     this.time=time;
    // }
    setPhone(phone){
        this.phone=phone;
    }
    validate(){
        if(this.user_count==undefined||this.phone==undefined){
            return false;
        }else{
            return true;
        }
    }
}

module.exports=CouponBean;