import React from 'react';

class ListTable extends React.Component{
    componentDidMount() {
        this.renderTable();
        this.handleClick=this.handleClick.bind(this);
    }
    handleClick(){
        console.log("Dd");
    }
    render(){
        let _this=this;
        let data=this.props.data;
        return (
            <div class="panel panel-default">
                <div class="panel-heading">{data.config.title} 리스트</div>
                <div class="panel-body">
                    <table data-toggle="table"
                           data-show-toggle={data.config.toggle} data-show-columns="true" data-search={data.config.search}
                           data-pagination={data.config.pagination} data-sort-name={data.config.sortBy}
                           data-sort-order={data.config.sortOrder}>
                        <thead>
                        <tr>
                            {data.header.map(function(item, index){
                                return (
                                    <th key={index} data-field={item.key} data-sortable="true">{item.name}</th>
                                );
                            })}
                            {data.config.hasButton.map(function(item, index){
                                return (
                                    <th key={index} data-field={item.key} data-sortable="false">{item.name}</th>
                                );
                            })}
                        </tr>
                        </thead>
                        <tbody>
                        {data.body.map(function(item, index){
                            let __this=_this;
                            return (
                                <tr  key={index}>
                                    {data.header.map(function(item2,index2){
                                        return (
                                            <td key={index2}>{item[item2.key]}</td>
                                        );
                                    })}
                                    {data.config.hasButton.map(function(item2, index2){
                                        let clickEvent=item2.onclick;
                                        {/*console.log(__this);*/}
                                        var boundClick = __this.handleClick.bind(this);
                                        return (
                                            <td key={index2}><button class="btn btn-primary" onClick={boundClick}>{item2.name}</button></td>
                                        );
                                    })}

                                </tr>
                            );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
    renderTable() {

        $('#hover, #striped, #condensed').click(function () {
            var classes = 'table';

            if ($('#hover').prop('checked')) {
                classes += ' table-hover';
            }
            if ($('#condensed').prop('checked')) {
                classes += ' table-condensed';
            }
            $('#table-style').bootstrapTable('destroy')
                .bootstrapTable({
                    classes: classes,
                    striped: $('#striped').prop('checked')
                });
        });
        $('[data-toggle="table"]').bootstrapTable();
    }
}
module.exports=ListTable;