import React from 'react';
import Quill from 'quill';
let BlockEmbed = Quill.import('blots/block/embed');
let Embed = Quill.import('blots/embed');

class VideoBlot extends BlockEmbed {
    static create(info) {
        let node = super.create();
        // Set non-format related attributes with static values
        node.setAttribute('src',info.url);
        if(info.class){
            node.setAttribute('src',info.class);
        }
        node.setAttribute('frameborder', '0');
        node.setAttribute('allowfullscreen', true);
        return node;
    }

    static formats(node) {
        // We still need to report unregistered embed formats
        let format = {};
        if (node.hasAttribute('height')) {
            format.height = node.getAttribute('height');
        }
        if (node.hasAttribute('width')) {
            format.width = node.getAttribute('width');
        }
        return format;
    }

    static value(node) {
        return node.getAttribute('src');
    }

    format(name, value) {
        // Handle unregistered embed formats
        if (name === 'height' || name === 'width') {
            if (value) {
                this.domNode.setAttribute(name, value);
            } else {
                this.domNode.removeAttribute(name, value);
            }
        } else if(name ==="align"){
            if(value==="center"){
                this.domNode.parentElement.style.textAlign="center";
            }else{
                this.domNode.parentElement.style.textAlign="initial";
            }
        } else{
            super.format(name, value);
        }
    }
}

class ImageBlot extends Embed {
    static create(value) {
        function nthIndex(str, pat, n){
            var L= str.length, i= -1;
            while(n-- && i++<L){
                i= str.indexOf(pat, i);
                if (i < 0) break;
            }
            return i;
        }
        let node = super.create(value);
        node.setAttribute('src', value.url);
        node.setAttribute('srcpart',value.url.substr(nthIndex(value.url,'/',6)));
        node.setAttribute('class',value.class);
        node.setAttribute('imageId',value.imageId);
        node.setAttribute('publicId',value.publicId);
        node.setAttribute('maxWidth',value.width);
        node.setAttribute('maxHeight',value.height);
        // node.style.width='100%';
        // node.style.height=100*value.height/value.width+"%";
        return node;

    }
}
class Editor extends React.Component {
    constructor(){
        super();
        this.editor=Object;
        this.state={
            body: ""
        };
        this.imageList=[];
        this.submit=this.submit.bind(this);
    }
    componentDidMount() {
        VideoBlot.blotName = 'video';
        VideoBlot.tagName = 'iframe';
        ImageBlot.blotName = 'image';
        ImageBlot.tagName = 'IMG';

        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction
            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'font': [] }],
            [{ 'align': [] }],
            ['link', 'image','video'],
            ['clean']    ];
        var options = {
            modules: {
                toolbar: {
                    container: toolbarOptions,
                    handlers: {
                        image: function () {
                            var elem = document.getElementById("theFile");
                            var _this = this;
                            if (elem && document.createEvent) {
                                var evt = document.createEvent("MouseEvents");
                                evt.initEvent("click", true, false);
                                elem.dispatchEvent(evt);
                                elem.addEventListener("change", imageListener.bind(_this, elem));
                            }
                        },
                        video: function() {
                            let range = this.quill.getSelection(true);
                            let url=prompt("주소를 입력하세요");
                            if(!url){
                               return;
                            }
                            this.quill.insertEmbed(range.index, 'video', {url:url}, Quill.sources.USER);
                            this.quill.formatText(range.index + 1, 1);
                            this.quill.setSelection(range.index + 1, Quill.sources.SILENT);
                        }
                    }
                }
            },
            readOnly: false,
            theme: 'snow'
        };
        var _this=this;
        function imageListener() {
            var __this = this;
            var elem = arguments[0];
            var range = __this.quill.getSelection();
            var form = $('form')[0];
            var formData = new FormData(form);
            formData.append("image", $("#theFile")[0].files[0]);
            $.ajax({
                url: 'http://localhost:3001/api/admin/filetest',
                processData: false,
                contentType: false,
                data: formData,
                type: 'POST',
                success: function (result) {
                    _this.imageList.push(result);
                    __this.quill.insertEmbed(range.index, 'image', {url:result.fileUrl,class:"uploaded_image",publicId:result.publicId, imageId: result.imageId, width: result.width, height: result.height}, Quill.sources.USER);
                    elem.removeEventListener("change", imageListener);
                }
            });
            elem.value = null;
            return false;
        }
        Quill.register(VideoBlot);
        Quill.register(ImageBlot);
        this.editor = new Quill('.post_editor', options);
        // this.editor.on('text-change',function(){
        //     let html=this.editor.root.innerHTML;
        //     this.setState({body:html});
        // }.bind(this));
    }
    submit(){
        // var sp1 = document.createElement("span");
        // sp1.innerHTML="image placeholder - "+$(".uploaded_image")[0].getAttribute("publicId");
       // $(".uploaded_image")[0].parentNode.insertBefore(sp1,$(".uploaded_image")[0]);
        this.props.onSubmit(this.editor.root.innerHTML,this.imageList);
    }
    render() {
        return (
            <div style={{background: 'white'}}>
                <form id="form" action="" method="post" encType="multipart/form-data" style={{display:'none'}}>
                    <input type="file" id="theFile" name="image"/>
                </form>
                <div id="toolbar"></div>
                <div class="post_editor" style={{'minHeight': '300px'}}></div>
                <button onClick={this.submit}>전송</button>
            </div>
        );
    }
}

export default Editor;