import React from 'react';
import {Router, Route, Link, browserHistory, IndexRoute} from 'react-router'
// import Footer from './Footer';
import cookie from 'react-cookie';
export default class Header extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#sidebar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">우리가 관리하는 페이지</a>
                            {/*<ul class="user-menu">*/}
                                {/*<li class="dropdown pull-right">*/}
                                    {/*<a href="#" class="dropdown-toggle" data-toggle="dropdown">*/}
                                        {/*<svg class="glyph stroked male-user">*/}
                                            {/*<use    xlinkHref="#stroked-male-user"></use>*/}
                                        {/*</svg>*/}
                                        {/*User <span class="caret"></span></a>*/}
                                    {/*<ul class="dropdown-menu" role="menu">*/}
                                        {/*<li><a href="#">*/}
                                            {/*<svg class="glyph stroked male-user">*/}
                                                {/*<use    xlinkHref="#stroked-male-user"></use>*/}
                                            {/*</svg>*/}
                                            {/*Profile</a></li>*/}
                                        {/*<li><a href="#">*/}
                                            {/*<svg class="glyph stroked gear">*/}
                                                {/*<use    xlinkHref="#stroked-gear"></use>*/}
                                            {/*</svg>*/}
                                            {/*Settings</a></li>*/}
                                        {/*<li><a href="#">*/}
                                            {/*<svg class="glyph stroked cancel">*/}
                                                {/*<use    xlinkHref="#stroked-cancel"></use>*/}
                                            {/*</svg>*/}
                                            {/*Logout</a></li>*/}
                                    {/*</ul>*/}
                                {/*</li>*/}
                            {/*</ul>*/}
                        </div>

                    </div>
                </nav>

                <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
                    <ul class="nav menu">
                        <li class="active"><Link to="/admin/home">
                            <svg class="glyph stroked dashboard-dial">
                                <use    xlinkHref="#stroked-dashboard-dial"></use>
                            </svg>
                            현황판</Link></li>
                        <li><Link to="/admin/post_upload">
                            <svg class="glyph stroked dashboard-dial">
                                <use xlinkHref="#stroked-dashboard-dial"></use>
                            </svg>
                            블로그 업로드</Link></li>
                        {/* <li><Link to="/admin/users">*/}
                            {/*<svg class="glyph stroked table">*/}
                                {/*<use    xlinkHref="#stroked-table"></use>*/}
                            {/*</svg>*/}
                            {/*유저목록</Link></li>*/}
                        {/*<li><Link to="/admin/coupon">*/}
                            {/*<svg class="glyph stroked pencil">*/}
                                {/*<use    xlinkHref="#stroked-pencil"></use>*/}
                            {/*</svg>*/}
                            {/*쿠폰 관리</Link></li>*/}
                        {/*<li><Link to="/admin/contact">*/}
                            {/*<svg class="glyph stroked pencil">*/}
                                {/*<use xlinkHref="#stroked-pencil"></use>*/}
                            {/*</svg>*/}
                            {/*씨리얼과의 대화</Link></li>*/}
                        {/*<li><a href="login.html">*/}
                            {/*<svg class="glyph stroked male-user">*/}
                                {/*<use    xlinkHref="#stroked-male-user"></use>*/}
                            {/*</svg>*/}
                            {/*Login Page</a></li>*/}
                    </ul>

                </div>
                {this.props.children}
            </div>
        )
    }
}






