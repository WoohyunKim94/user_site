import React from 'react';
import Editor from '../component/Editor';
import PostShow from '../../user_view/post/PostShow';

class EditorContainer extends React.Component {
    constructor() {
        super();
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(html, images) {
        this.props.onSubmit(html, images);
    }

    render() {
        var props = {
            onSubmit: this.onSubmit
        };
        return (
            <div>
                <Editor {...props}/>
            </div>
        )
    }
}
export default class PostUpload extends React.Component {
    constructor() {
        super();
        this.state = {
            body: '',
            title: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.titleChange = this.titleChange.bind(this);
    }

    componentDidMount() {
    }

    onSubmit(html, images) {
        this.setState({body: html},function(){
            if (this.state.body == '' || this.state.title == '') {
                alert('제목과 내용을 모두 채워주세요');
                return;
            }
            let data = {
                body: this.state.body,
                images: images,
                title: this.state.title
            };
            //서버로 전송
            $.ajax({
                type: 'POST',
                url: '/api/post/upload',
                data: data
            })
                .done(function (data) {
                    console.log(data);
                })
                .fail(function (jqXhr) {
                    console.log('failed to register');
                });
        });
    }

    titleChange(e) {
        this.setState({title: e.target.value});
    }

    render() {
        return (

            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="#">
                            <svg class="glyph stroked home">
                                <use xlinkHref="#stroked-home"></use>
                            </svg>
                        </a></li>
                        <li class="active">블로그 업로드</li>
                    </ol>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <label for="title">제목</label>
                            <input type="text" id="title" onChange={this.titleChange}/>
                            <EditorContainer onSubmit={this.onSubmit}/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        );
    }
}