import express from 'express';
import Post from '../models/Post';
import Bookshelf from '../models/config';
import Post_image from '../models/Post_image';
import Image from '../models/Image';
const router = express.Router();


router.get('/list', (req, res)=> {
    Post.query('orderBy', 'created_at', 'desc').fetchPage({
        columns: ['id', 'title', Bookshelf.knex.raw("SUBSTRING_INDEX(`body`, '</p>', 2) as body"), 'created_at'],
        pageSize: 3,
        page: req.query.page,
    }).then(posts=> {
        let postList = posts.toJSON();
        let count = 0;
        postList.forEach((item, index) => {
            //각 포스트마다 이미지 불러오기
            let postId = item.id;
            Post_image.where({
                Posts_id: postId,
                isMain: true
            }).fetch().then(mainImage=> {
                let imageId = mainImage.toJSON().id;
                Image.where({
                    id: imageId
                }).fetch().then(image=> {
                    postList[index].image = image.toJSON().imageId;
                    if (++count == postList.length) {
                        res.json({
                            posts: postList,
                            hasMorePage: (posts.pagination.pageCount > req.query.page ? true : false)
                        });
                    }
                });

            });
        });
    });
});
// router.get('/')
router.get('/open', (req, res)=> {
    Post.where({id: req.query.id}).fetch().then(post=> {
        res.json(post);
    });
});
router.post('/upload', (req, res)=> {
    new Post({
        title: req.body.title,
        body: req.body.body
    }).save().then(result=> {
        let postId = result.toJSON().id;
        let images = req.body.images;
        for (let i = 0; i < images.length; i++) {
            new Post_image({
                Posts_id: postId,
                Images_id: images[i].id
            }).save();
        }
        new Post_image({
            Posts_id: postId,
            Images_id: req.body.main_image.id,
            isMain: true
        }).save().then(()=> {
            res.json({result: result});
        });
    })
});
/*
 response

 {
 "posts": [
 {
 "id": 2,
 "title": "test_post2",
 "body": null,
 "created_at": "2016-09-17T08:37:41.000Z"
 },
 {
 "id": 1,
 "title": "test_post",
 "body": "sentence1",
 "created_at": "2016-09-17T06:36:32.000Z"
 }
 ],
 "hasMorePage": false
 }

 */

export default router;

