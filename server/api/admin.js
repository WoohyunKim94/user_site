import express from 'express';
import User from '../models/User';
import Store from '../models/Store';
import multer from 'multer';
import cloudinary from '../modules/cloudinary';


const router = express.Router();
var storage = multer.memoryStorage();
var upload = multer({storage: storage});

router.get('/', (req, res)=> {
    // User.byName('woohyun').then(u => {
    //     res.json(u);
    // });
    // Store.getFeaturedPosts(1).then(function(d){
    //     res.json(d);
    // });
    // let tag=(cloudinary.image("twpicxb0femqfr21dmvc.png", {width: 100, height: 150, crop: 'fill'}));
    // console.log(tag);
    // res.json({result:tag});
});
router.post('/filetest', upload.any(), (req, res)=> {
    cloudinary._upload(req).then(function(result){
       res.json({
           fileUrl:result.url,
           publicId:result.public_id,
           imageId: result.imageId,
           width: result.width,
           height: result.height
       });
    });
});

export default router;



