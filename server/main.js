import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';


const app = express();
const port = 3000;
const devPort = 3001;
app.use(bodyParser());
app.use(cookieParser());

if (process.env.NODE_ENV == 'development') {
    console.log('Server is running on development mode');
    const WebpackDevServer=require('webpack-dev-server');
    const webpack = require('webpack');
    const config = require('../webpack.dev.config');
    let compiler = webpack(config);
    let devServer = new WebpackDevServer(compiler, config.devServer);
    devServer.listen(devPort, () => {
        console.log('webpack-dev-server is listening on port', devPort);
    });
}

import admin from './routes/admin';
import owner from './routes/owner';
import api_admin from './api/admin';
import api_post from './api/post';
import api_owner from './api/owner';

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.header('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.header('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});
app.use('/admin', admin);
app.use('/owner',owner);
app.use('/api/admin',api_admin);
app.use('/api/post',api_post);
app.use('/api/owner',api_owner);
app.use('/',express.static(__dirname + '/../public'));
app.get('*', function (request, response) {
    response.sendFile(path.resolve(__dirname, '../public', 'index.html'))
});


const server = app.listen(process.env.PORT||port, () => {
    console.log('Express listening on port', port);
});
