'use strict';

import Bookshelf from './config';

require('./User');

var Opinion = Bookshelf.Model.extend({
    tableName: 'Opinions',
    hasTimestamps: true,
    User: function(){
        return this.belongsTo('User','Users_Id');
    }
},{

});

module.exports = Bookshelf.model('Opinion', Opinion);