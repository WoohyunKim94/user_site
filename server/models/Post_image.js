'use strict';

import Bookshelf from './config';


var Post_image = Bookshelf.Model.extend({
    tableName: 'Post_images',
    hasTimestamps: true

},{

});

module.exports = Bookshelf.model('Post_image', Post_image);