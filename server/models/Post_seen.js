'use strict';

import Bookshelf from './config';

require('./User');
require('./Post');

var Post_seen = Bookshelf.Model.extend({
    tableName: 'Post_seens',
    hasTimestamps: true,
    User: function(){
        return this.belongsTo('User');
    },
    Post: function(){
        return this.belongsTo('Post');
    }
},{

});

module.exports = Bookshelf.model('Post_seen', Post_seen);