'use strict';

import Bookshelf from './config';

require('./Store');

var Owner = Bookshelf.Model.extend({
    tableName: 'Owners',
    hasTimestamps: true,
    Stores: function(){
        return this.hasMany('Store');
    }
},{

});

module.exports = Bookshelf.model('Owner', Owner);