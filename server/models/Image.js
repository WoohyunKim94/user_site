'use strict';

import Bookshelf from './config';


var Image = Bookshelf.Model.extend({
    tableName: 'Images',
    hasTimestamps: true

},{

});

module.exports = Bookshelf.model('Image', Image);