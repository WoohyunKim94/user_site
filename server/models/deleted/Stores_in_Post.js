'use strict';

import Bookshelf from '../config';
require('./../Store');
require('./../Post');

var Stores_in_Post = Bookshelf.Model.extend({
    tableName: 'Stores_in_Posts',
    hasTimestamps: true,
    Store: function(){
        return this.belongsTo('Store','Stores_id');
    },
    Post: function(){
        return this.belongsTo('Post','Posts_id');
    }
},{

});

module.exports = Bookshelf.model('Stores_in_Post', Stores_in_Post)