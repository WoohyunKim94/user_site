'use strict';

import Bookshelf from './config';

require('./Coupon');
var Post=require('./Post');

require('./Owner');

var Store = Bookshelf.Model.extend({
    tableName: 'Stores',
    hasTimestamps: true,
    Coupons: function () {
        return this.hasMany('Coupon', 'Stores_id');
    },
    Owner: function(){
        return this.belongsTo('Owner');
    },
    Post: function(){
        return this.belongsTo('Post');
    }
}, {
    // getFeaturedPosts: function (storeId) {
    //     return new Promise(function(resolve,reject){
    //         Store.where({id: storeId}).fetch({withRelated: ['Stores_in_Post']}).then(function (d) {
    //             var post_list=[];
    //             d.toJSON().Stores_in_Post.forEach(function (item) {
    //                 post_list.push(item.Posts_id);
    //             });
    //             Post.where('id','in',post_list).fetchAll().then(function(result){
    //                 resolve(result.toJSON());
    //             });
    //         });
    //     });
    //     // attribute 제한 설정하는 거
    // }
});

module.exports = Bookshelf.model('Store', Store);