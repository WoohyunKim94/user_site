'use strict';

var knex = require('knex')({
    client: 'mysql',
    connection: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'mydb',
        charset: 'utf8'
    },
    debug: true
});
var bookshelf = require('bookshelf')(knex);

bookshelf.plugin('registry'); // Resolve circular dependencies with relations
bookshelf.plugin(require('bookshelf-soft-delete'));
bookshelf.plugin('pagination');

module.exports = bookshelf;