'use strict';

import Bookshelf from './config';
require('./Store');

var Post = Bookshelf.Model.extend({
    tableName: 'Posts',
    hasTimestamps: true,
},{

});

module.exports = Bookshelf.model('Post', Post);