'use strict';

import Bookshelf from './config';

require('./Coupon_usage');
require('./Store');

var Coupon = Bookshelf.Model.extend({
    tableName: 'Coupons',
    hasTimestamps: true,
    Coupon_usages: function(){
        return this.hasMany('Coupon_usage');
    },
    Store: function(){
        return this.belongsTo('Store');
    }
},{

});

module.exports = Bookshelf.model('Coupon', Coupon);