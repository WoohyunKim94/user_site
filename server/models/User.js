'use strict';

import Bookshelf from './config';

var User = Bookshelf.Model.extend({
    tableName: 'Users',
    hasTimestamps: true,
    Coupon_usages: function(){
        return this.hasMany('Coupon_usage','Users_id');
    },
    Post_seens: function(){
        return this.hasMany('Post_seen','Users_id');
    },
    Comments: function(){
        return this.hasMany('Comment','Users_id');
    },
    Opinion: function(){
        return this.hasMany('Opinion','Users_id');
    }
},{
    byName: function(name){
        return User.where({name:name}).fetch();
    }
});

module.exports = Bookshelf.model('User', User);