'use strict';

import Bookshelf from './config';

require('./User');
require('./Coupon');

var Coupon_usage = Bookshelf.Model.extend({
    tableName: 'Coupon_usages',
    hasTimestamps: true,
    User: function(){
        return this.belongsTo('User');
    },
    Coupon:function(){
        return this.belongsTo('Coupon');
    }
},{

});

module.exports = Bookshelf.model('Coupon_usage', Coupon_usage);