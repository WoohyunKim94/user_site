'use strict';

import Bookshelf from './config';

require('./User');
require('./Post');

var Comment = Bookshelf.Model.extend({
    tableName: 'Comments',
    hasTimestamps: true,
    User: function(){
        return this.belongsTo('User');
    },
    Post: function(){
        return this.belongsTo('Post');
    }
},{

});

module.exports = Bookshelf.model('Comment', Comment);