import cloudinary from 'cloudinary';
import Datauri from 'datauri';
import path from 'path';
import Image from '../models/Image';

cloudinary.config({
    cloud_name: 'seereal',
    api_key: '848649886355129',
    api_secret: 'YWi__2aPpepsOKv0myOwyGpE8mk'
});

cloudinary._upload=function(req){
    return new Promise(function(resolve, reject){
        let duri=new Datauri();
        duri.format(path.extname(req.files[0].originalname).toString(), req.files[0].buffer);
        cloudinary.uploader.upload(duri.content,function(result){
            new Image({imageId:result.public_id, width: result.width, height: result.height}).save().then(function(saveResult){
                result.imageId=saveResult.id;
                resolve(result);
            });
            // res.json({result:result, fileUrl: result.url});
        },function(err){
            reject(err);
        });
    });
}

cloudinary._getImage=function(imageId){
    return cloudinary.image(imageId);
};

module.exports = cloudinary;